package com.vaapglkns.Activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vaapglkns.Global.*
import com.vaapglkns.Model.FBItem
import com.vaapglkns.Model.LoginItem
import com.vaapglkns.Model.RegisterItem
import com.vaapglkns.Model.Spinner
import com.vaapglkns.R
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.row_get_email_popup.view.*
import org.json.JSONObject
import java.io.IOException
import java.util.*

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
class LoginActivity : BaseActivity(), View.OnClickListener, Global.DialogClickListener,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    lateinit var spinnerSelData: ArrayList<Spinner>

    //FOR GET CURRENT LOCATION OF USER
    private val CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var currentLatitude: Double = 0.toDouble()
    private var currentLongitude: Double = 0.toDouble()

    //FOR FACEBOOK LOGIN
    private lateinit var callbackManager: CallbackManager
    private lateinit var alertGetEmail: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initDrawerMenu(true)
        initBackPress(false)
        initializeWidget()
        setWidgetOperations()
        setData()
        getLocation()
        initFacebook()
//        Global.getHashKey(getActivity())

        if (Debug.DEBUG) {
            edEmail.setText("ajay.eih@gmail.com")
            edPassword.setText("123456")
        }
    }

    //region FOR INITIALIZE WIDGET
    private fun initializeWidget() {
        setTitleText("Log In")
    }
    //endregion

    //region FOR GET LOCATION
    private fun getLocation() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        //CREATE THE LOCATION REQUEST OBJECT
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10 * 1000)        // 10 seconds, in milliseconds
            .setFastestInterval(1 * 1000) // 1 second, in milliseconds
    }
    //endregion

    //region FOR SET DATA
    private fun setData() {
        spinnerSelData = ArrayList<Spinner>()
        spinnerSelData.add(Spinner("0", "0"))
        spinnerSelData.add(Spinner("1", "1"))
        spinnerSelData.add(Spinner("2", "2"))
        spinnerSelData.add(Spinner("3", "3"))
        spinnerSelData.add(Spinner("4", "4"))
        spinnerSelData.add(Spinner("5", "5"))
        spinnerSelData.add(Spinner("6", "6"))
        spinnerSelData.add(Spinner("7", "7"))
    }
    //endregion

    //region FOR INITIALIZE WIDGET
    private fun initFacebook() {
        FacebookSdk.sdkInitialize(getActivity())
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().logOut()
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    getFacebookProfile(loginResult.accessToken)
                }

                override fun onCancel() {
                    showToast(getString(R.string.fb_cancel), Toast.LENGTH_SHORT)
                    Debug.e("FB", "onCancel")
                }

                override fun onError(error: FacebookException) {
                    Debug.e("FB", "onError : " + error.message)
                    showToast(getString(R.string.fb_login_err), Toast.LENGTH_SHORT)
                }
            })
    }
    //endregion

    //region FOR SET WIDGET OPERATIONS
    private fun setWidgetOperations() {
        tvForgotPassword.setOnClickListener(this)
        btnSignIn.setOnClickListener(this)
        ivFBLogin.setOnClickListener(this)
        ivAddUser.setOnClickListener(this)
    }
    //endregion

    //region FOR VALIDATE LOGIN DETAILS
    private fun validateLoginDetails(email: String, password: String): Boolean {
        if (!TextUtils.isEmpty(email)) {
            if (!TextUtils.isEmpty(password)) {
                if (Global.validateEmail(email)) {
                    if (password.length >= 5) {
                        return true
                    } else {
                        showToast("Please enter valid Password.!")
                    }
                } else {
                    showToast("Please enter valid E-mail address.!")
                }
            } else {
                showToast("Please enter your Password.!")
            }
        } else {
            showToast("Please enter your E-mail address.!")
        }
        return false
    }
    //endregion

    //region FOR GET FACEBOOK PROFILE
    private fun getFacebookProfile(accessToken: AccessToken) {
        val request = GraphRequest.newMeRequest(accessToken) { `object`, response ->
            Debug.e("getFacebookProfile", "" + `object`.toString())
            try {
                val fbRes =
                    Gson().fromJson<FBItem>(`object`.toString(), object : TypeToken<FBItem>() {
                    }.type)

                Debug.e("Fb Login #", response.toString())
                Debug.e("Fb Login #", `object`.toString())

                var id = ""
                var birthday = ""
                var email = ""
                var first_name = ""
                var last_name = ""
                var gender = ""
                var image_url = ""
                var country = ""
                var age = ""

                if (`object`.has("id")) {
                    id = `object`.optString("id")
                    image_url = "http://graph.facebook.com/$id/picture?type=large"
                }

                if (`object`.has("birthday")) {
                    birthday = Global.changeDateFormat(
                        `object`.optString("birthday"),
                        "dd/MM/yyyy",
                        "yyyy-MM-dd"
                    )
                    age = Global.getAge(
                        Integer.parseInt(
                            Global.changeDateFormat(
                                `object`.optString("birthday"),
                                "dd/MM/yyyy",
                                "yyyy"
                            )
                        ),
                        Integer.parseInt(
                            Global.changeDateFormat(
                                `object`.optString("birthday"),
                                "dd/MM/yyyy",
                                "MM"
                            )
                        ),
                        Integer.parseInt(
                            Global.changeDateFormat(
                                `object`.optString("birthday"),
                                "dd/MM/yyyy",
                                "dd"
                            )
                        )
                    )
                }

                if (`object`.has("email")) {
                    email = `object`.optString("email")
                }

                if (`object`.has("first_name")) {
                    first_name = `object`.optString("first_name")
                }

                if (`object`.has("last_name")) {
                    last_name = `object`.optString("last_name")
                }

                if (`object`.has("gender")) {
                    gender = `object`.optString("gender")
                }

                if (`object`.has("location")) {
                    country = `object`.optJSONObject("location").optString("name")
                }

                if (!TextUtils.isEmpty(email)) {
                    fbLogin(id, email, first_name, last_name)
                } else {
                    showGetEmailPopup(id, first_name, last_name)
                }
                Debug.e("email #", "email #$email")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val parameters = Bundle()
        parameters.putString(
            "fields",
            "id, name, first_name, last_name, email,birthday,gender,location,picture.type(large)"
        )
        request.parameters = parameters
        request.executeAsync()
    }
    //endregion

    //region FOR SHOW GET EMAIL POPUP
    private fun showGetEmailPopup(id: String, firstName: String, lastName: String) {
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.row_get_email_popup, null)

        alertLayout.btnRowSubmit.setOnClickListener {
            val emailAddress = alertLayout.edRowEmail.text.toString()
            if (!TextUtils.isEmpty(emailAddress)) {
                if (Global.isValidEmail(emailAddress)) {
                    alertGetEmail.dismiss()

                    fbLogin(id, emailAddress, firstName, lastName)
                } else {
                    showToast("Please enter valid Email Address!")
                }
            } else {
                showToast("Please enter your Email Address!")
            }
        }

        val alert = AlertDialog.Builder(this)
        alert.setView(alertLayout)
        alert.setCancelable(false)
        alertGetEmail = alert.create()
        try {
            alertGetEmail.window!!.attributes.windowAnimations = R.style.FadIn_FadOutAnimation
            if (!alertGetEmail.isShowing) {
                alertGetEmail.show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    //endregion

    //region CALL SERVICE FOR FB LOGIN
    private fun fbLogin(id: String, email: String, firstName: String, lastName: String) {
        try {
            showDialog("")

            val bodyParameter = HashMap<String, String>()
            bodyParameter[RequestParamsUtils.USERNAME] = email
            bodyParameter[RequestParamsUtils.PASSWORD] = id
            bodyParameter[RequestParamsUtils.DEVICE_TOKEN] = "vaapglkns88888888"
            bodyParameter[RequestParamsUtils.DEVICE_TYPE] = "1"
            bodyParameter[RequestParamsUtils.DEVICE_ID] = Global.getDeviceId(getActivity())

            Debug.e("Parameters: ", JSONObject(bodyParameter).toString())
            val retrofit = RetrofitHttpRequest.newRequestRetrofit(getActivity())
            val retrofitService = retrofit.create(RetrofitUrls::class.java)
            retrofitService.login(bodyParameter).enqueue(FBLoginDataHandler(getActivity(), id, email, firstName, lastName))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class FBLoginDataHandler(
        context: Activity,
        val id: String,
        val email: String,
        val firstName: String,
        val lastName: String
    ) : RetrofitResponseHandler(context) {

        override fun onStart() {

        }

        override fun onSuccess(content: String) {
            dismissDialog()
            try {
                val response = XmlToJson.Builder(content).build()
                Debug.e("getLoginData  #", response.toFormattedString())
                //var personMap: Map<String, Any> = gson.fromJson(json, object : TypeToken<Map<String, Any>>() {}.type)

                if (content != null) {
                    val res = Gson().fromJson<LoginItem>(
                        response.toString(),
                        object : TypeToken<LoginItem>() {}.type
                    )

                    if (res.message.status == "success") {
                        showToast(res.message.text)

                        //FOR SAVE USER TOKEN AND USE IN SHARED PREFERENCE
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_TOKEN, "vaapglkns")
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_ID, "vaapglkns")

//                        startNewActivity(Intent(getActivity(), MainActivity::class.java))
//                        finishAffinity()
                    } else {
                        registerUser(firstName, lastName, email, id)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onFinish() {
            dismissDialog()
        }

        override fun onFailure(e: Throwable, content: String) {
            dismissDialog()
        }
    }
    //endregion

    //region CALL SERVICE FOR REGISTER USER
    private fun registerUser(firstName: String, lastName: String, email: String, password: String) {
        try {
            showDialog("")

            val bodyParameter = HashMap<String, String>()
            bodyParameter[RequestParamsUtils.FIRSTNAME] = firstName
            bodyParameter[RequestParamsUtils.LASTNAME] = lastName
            bodyParameter[RequestParamsUtils.EMAIL] = email
            bodyParameter[RequestParamsUtils.PASSWORD] = password
            bodyParameter[RequestParamsUtils.CONFIRMATION] = password
            bodyParameter[RequestParamsUtils.IS_SUBSCRIBED] = "true"
            bodyParameter[RequestParamsUtils.DEVICE_TOKEN] = "vaapglkns88888888"
            bodyParameter[RequestParamsUtils.DEVICE_TYPE] = "1"
            bodyParameter[RequestParamsUtils.DEVICE_ID] = Global.getDeviceId(getActivity())

            Debug.e("Parameters: ", JSONObject(bodyParameter).toString())
            val retrofit = RetrofitHttpRequest.newRequestRetrofit(getActivity())
            val retrofitService = retrofit.create(RetrofitUrls::class.java)
            retrofitService.register(bodyParameter).enqueue(GetRegisterDataHandler(getActivity()))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class GetRegisterDataHandler(context: Activity) :
        RetrofitResponseHandler(context) {

        override fun onStart() {

        }

        override fun onSuccess(content: String) {
            dismissDialog()
            try {
                val response = XmlToJson.Builder(content).build()
                Debug.e("getRegisterData  #", response.toFormattedString())

                if (content != null) {
                    val res = Gson().fromJson<RegisterItem>(
                        JSONObject(response.toFormattedString()).toString(),
                        object : TypeToken<RegisterItem>() {}.type
                    )

                    if (res.message.status == "success") {
                        showToast(res.message.text)

//                        startNewActivity(Intent(getActivity(), LoginActivity::class.java))
//                        finishAffinity()

                        //FOR SAVE USER TOKEN AND USE IN SHARED PREFERENCE
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_TOKEN, "vaapglkns")
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_ID, "vaapglkns")

//                        startNewActivity(Intent(getActivity(), MainActivity::class.java))
//                        finishAffinity()
                    } else {
                        showToast(res.message.text)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onFinish() {
            dismissDialog()
        }

        override fun onFailure(e: Throwable, content: String) {
            dismissDialog()
        }
    }
    //endregion

    //region CALL SERVICE FOR USER LOGIN
    private fun userLogin(email: String, password: String) {
        try {
            showDialog("")
            //       val jsonObject = JsonObject()
//            jsonObject.addProperty("token", "312a8e979139d0d1a3da78ca05251db5")
            //            jsonObject.addProperty(RequestParamsUtils.ADDRESS_TAG, address_tag);
            //            jsonObject.addProperty(RequestParamsUtils.ADDRESS_LINE1, addressLine1);

            val bodyParameter = HashMap<String, String>()
            bodyParameter[RequestParamsUtils.USERNAME] = email
            bodyParameter[RequestParamsUtils.PASSWORD] = password
            bodyParameter[RequestParamsUtils.DEVICE_TOKEN] = "vaapglkns88888888"
            bodyParameter[RequestParamsUtils.DEVICE_TYPE] = "1"
            bodyParameter[RequestParamsUtils.DEVICE_ID] = Global.getDeviceId(getActivity())

            Debug.e("Parameters: ", JSONObject(bodyParameter).toString())
            val retrofit = RetrofitHttpRequest.newRequestRetrofit(getActivity())
            val retrofitService = retrofit.create(RetrofitUrls::class.java)
            retrofitService.login(bodyParameter).enqueue(GetUserLoginDataHandler(getActivity(), email, password))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class GetUserLoginDataHandler(context: Activity, val email: String, val password: String) :
        RetrofitResponseHandler(context) {

        override fun onStart() {

        }

        override fun onSuccess(content: String) {
            dismissDialog()
            try {
                val response = XmlToJson.Builder(content).build()
                Debug.e("getLoginData  #", response.toFormattedString())
                //var personMap: Map<String, Any> = gson.fromJson(json, object : TypeToken<Map<String, Any>>() {}.type)

                if (content != null) {
                    val res = Gson().fromJson<LoginItem>(response.toString(), object : TypeToken<LoginItem>() {}.type)

                    if (res.message.status == "success") {
//                        loadLogin(email, password)
                        showToast(res.message.text)

                        //FOR SAVE USER TOKEN AND USE IN SHARED PREFERENCE
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_TOKEN, "vaapglkns")
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_ID, "vaapglkns")

//                        startNewActivity(Intent(getActivity(), MainActivity::class.java))
//                        finishAffinity()
                    } else {
                        showToast(res.message.text)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onFinish() {
            dismissDialog()
        }

        override fun onFailure(e: Throwable, content: String) {
            dismissDialog()
        }
    }
    //endregion

    //region CALL SERVICE FOR LOAD LOGIN
    private fun loadLogin(email: String, password: String) {
        try {
            showDialog("")
            //       val jsonObject = JsonObject()
//            jsonObject.addProperty("token", "312a8e979139d0d1a3da78ca05251db5")
            //            jsonObject.addProperty(RequestParamsUtils.ADDRESS_TAG, address_tag);
            //            jsonObject.addProperty(RequestParamsUtils.ADDRESS_LINE1, addressLine1);

            val bodyParameter = HashMap<String, String>()
            bodyParameter[RequestParamsUtils.USERNAME] = email
            bodyParameter[RequestParamsUtils.PASSWORD] = password
            bodyParameter[RequestParamsUtils.DEVICE_TOKEN] = "vaapglkns88888888"
            bodyParameter[RequestParamsUtils.DEVICE_TYPE] = "1"
            bodyParameter[RequestParamsUtils.DEVICE_ID] = Global.getDeviceId(getActivity())

            Debug.e("Parameters: ", JSONObject(bodyParameter).toString())
            val retrofit = RetrofitHttpRequest.newRequestRetrofit(getActivity())
            val retrofitService = retrofit.create(RetrofitUrls::class.java)
            retrofitService.loadLogin(bodyParameter).enqueue(GetLoadLoginDataHandler(getActivity()))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class GetLoadLoginDataHandler(context: Activity) :
        RetrofitResponseHandler(context) {

        override fun onStart() {

        }

        override fun onSuccess(content: String) {
            dismissDialog()
            try {
                val response = XmlToJson.Builder(content).build()
                Debug.e("getLoginData  #", response.toFormattedString())
                //var personMap: Map<String, Any> = gson.fromJson(json, object : TypeToken<Map<String, Any>>() {}.type)

                if (content != null) {
                    val res = Gson().fromJson<LoginItem>(response.toString(), object : TypeToken<LoginItem>() {}.type)

                    if (res.message.status == "success") {
                        showToast(res.message.text)

                        //FOR SAVE USER TOKEN AND USE IN SHARED PREFERENCE
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_TOKEN, "vaapglkns")
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_ID, "vaapglkns")

//                        startNewActivity(Intent(getActivity(), MainActivity::class.java))
//                        finishAffinity()
                    } else {
                        showToast(res.message.text)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onFinish() {
            dismissDialog()
        }

        override fun onFailure(e: Throwable, content: String) {
            dismissDialog()
        }
    }
    //endregion

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tvForgotPassword -> {
//                Global.showSingleDialog(getActivity(), "Forgot Password?", "Click OK to reset your Password!")
//                Global.setDialogListner(this)
//                startNewActivity(Intent(getActivity(), ForgotPasswordActivity::class.java))
            }

            R.id.btnSignIn -> {
                val email: String = edEmail.text.toString()
                val password: String = edPassword.text.toString()
                if (validateLoginDetails(email, password)) {
                    userLogin(email, password)
                }
            }

            R.id.ivFBLogin -> {
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "email"))
            }

            R.id.ivAddUser -> {
                if (getActivity() is RegisterActivity) {
                    hideDrawerMenu(true)
                } else {
                    val intent = Intent(getActivity(), RegisterActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    startNewActivity(intent)
                    hideDrawerMenu(true)
                }
            }
        }
    }

    override fun onNegetiveClick(dialog: DialogInterface) {
        showToast("VAAPGLKNS")
    }

    override fun onPositiveClick(dialog: DialogInterface) {
        showToast("VAAPGLKNS")
    }

    override fun onResume() {
        try {
            mGoogleApiClient!!.connect()
        } catch (e: KotlinNullPointerException) {
            e.printStackTrace()
        }
        super.onResume()
    }

    override fun onPause() {
        try {
            if (mGoogleApiClient!!.isConnected) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
                mGoogleApiClient!!.disconnect()
            }
        } catch (e: KotlinNullPointerException) {
            e.printStackTrace()
        }
        super.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    //region OVERRIDE METHODS FOR LOCATION
    override fun onLocationChanged(location: Location?) {
        currentLatitude = location!!.latitude
        currentLongitude = location.longitude

        val geoCoder = Geocoder(getActivity(), Locale.getDefault())
        val addresses: MutableList<Address>? =
            geoCoder.getFromLocation(currentLatitude, currentLongitude, 1)
        val strCityName = addresses!![0].locality
        val strStateName = addresses[0].adminArea
        val strAddress = addresses[0].getAddressLine(0)
        val strCountryName = addresses[0].getAddressLine(2)
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST
                )
            } catch (e: IntentSender.SendIntentException) {
                e.printStackTrace()
            }
        } else {
            Log.e(
                "Error",
                "Location services connection failed with code " + connectionResult.errorCode
            )
        }
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        try {
            val location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
                )
            } else {
                //IF EVERYTHING WENT FINE LETS GET LATITUDE AND LONGITUDE
                currentLatitude = location.latitude
                currentLongitude = location.longitude

                val geoCoder = Geocoder(getActivity(), Locale.getDefault())
                val addresses: MutableList<Address>? =
                    geoCoder.getFromLocation(currentLatitude, currentLongitude, 1)
                val strCityName = addresses!![0].locality
                val strStateName = addresses[0].adminArea
                val strAddress = addresses[0].getAddressLine(0)
                val strCountryName = addresses[0].getAddressLine(2)


            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.e("Error", "Location services connection suspended..!")
    }
    //endregion
}
