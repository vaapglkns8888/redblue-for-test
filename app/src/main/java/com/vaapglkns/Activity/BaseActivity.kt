package com.vaapglkns.Activity

import android.annotation.TargetApi
import android.app.Activity
import android.app.Dialog
import android.content.*
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager


import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.vaapglkns.Adapter.SpinnerSelAdapter
import com.vaapglkns.BuildConfig
import com.vaapglkns.Global.*
import com.vaapglkns.Model.LoginItem
import com.vaapglkns.Model.Spinner
import com.vaapglkns.R
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import kotlinx.android.synthetic.main.drawer_menu_footer_main.view.*
import kotlinx.android.synthetic.main.drawer_menu_header_main.view.*
import kotlinx.android.synthetic.main.topbar.*
import org.json.JSONObject
import java.io.File
import java.text.DecimalFormat
import java.util.HashMap

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
open class BaseActivity : AppCompatActivity() {
    private var ad: RetrofitProgressDialog? = null
    private lateinit var commonReceiver: MyEventServiceReceiver
    private lateinit var toast: Toast
    private val REQUEST_CODE_GALLERY = 1
    private val REQUEST_CODE_TAKE_PICTURE = 2
    private val TEMP_PHOTO_FILE_NAME = "temp_photo.jpg"
    lateinit var mFileTemp: File
    lateinit var mImageCaptureUri: Uri
    private var marshMallowPermission = MarshMallowPermission(getActivity())
    private lateinit var result: Drawer

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        toast = Toast.makeText(getActivity(), "", Toast.LENGTH_LONG)

        val intentFilter = IntentFilter()
        intentFilter.addAction(StaticDataUtility.FINISH_ACTIVITY)

        commonReceiver = MyEventServiceReceiver()
        LocalBroadcastManager.getInstance(this).registerReceiver(commonReceiver, intentFilter)
    }

    internal inner class MyEventServiceReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                if (intent.action!!.equals(StaticDataUtility.FINISH_ACTIVITY, ignoreCase = true)) {
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getActivity(): BaseActivity {
        return this
    }

    fun showDialog(msg: String) {
        try {
            if (ad != null && ad!!.isShowing) {
                return
            }
            ad = RetrofitProgressDialog.getInstant(getActivity())
            ad!!.show(msg)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun dismissDialog() {
        try {
            if (ad != null) {
                ad!!.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showToast(text: String, duration: Int) {
        runOnUiThread {
            toast.setText(text)
            toast.duration = duration
            toast.show()
        }
    }

    fun showToast(text: String) {
        runOnUiThread {
            toast.setText(text)
            toast.duration = Toast.LENGTH_SHORT
            toast.show()
        }
    }

    fun finishActivity() {
//        if (getActivity() is MainActivity) {
//
//        } else {
//            getActivity().finish()
//        }
        getActivity().finish()
    }

    //region FOR START NEW ACTIVITY
    fun startNewActivity(i: Intent) {
        startActivity(i)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
    //endregion

    //region FOR START NEW ACTIVITY
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    fun startNewActivity(i: Intent, bundle: Bundle) {
        i.putExtras(bundle)
        startActivity(i)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
    //endregion

    //region START NEW ACTIVITY FOR RESULT
    fun startNewActivityForResult(i: Intent, requestCode: Int) {
        startActivityForResult(i, requestCode)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
    //endregion

    //region SHOW POPUP FOR CONFIRM LOGOUT
    fun confirmLogout() {
        val builder = AlertDialog.Builder(getActivity())
            .setTitle(getActivity().resources.getString(R.string.logout_title))
            .setMessage(getActivity().resources.getString(R.string.logout_msg))
            .setPositiveButton(
                getActivity().resources.getString(R.string.btn_yes),
                DialogInterface.OnClickListener { dialog, which ->
                    //showToast(getActivity().getString(R.string.logout), Toast.LENGTH_SHORT);
                    //LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(Intent(StaticDataUtility.FINISH_ACTIVITY))
                    //val intent = Intent(getActivity(), LoginActivity::class.java)
                    //startActivity(intent)

                    logout()
                })
            .setNegativeButton(
                getActivity().resources.getString(R.string.btn_no),
                DialogInterface.OnClickListener { dialog, which -> })
        val dialog = builder.create()
        dialog.show()
    }
    //endregion

    //region CALL SERVICE FOR USER LOGOUT
    private fun logout() {
        try {
            showDialog("")

            val bodyParameter = HashMap<String, String>()
            bodyParameter[RequestParamsUtils.DEVICE_TOKEN] = "vaapglkns88888888"
            bodyParameter[RequestParamsUtils.DEVICE_TYPE] = "1"
            bodyParameter[RequestParamsUtils.DEVICE_ID] = Global.getDeviceId(getActivity())

            Debug.e("Parameters: ", JSONObject(bodyParameter).toString())
            val retrofit = RetrofitHttpRequest.newRequestRetrofit(getActivity())
            val retrofitService = retrofit.create(RetrofitUrls::class.java)
            retrofitService.logout(bodyParameter).enqueue(GetLogoutDataHandler(getActivity()))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class GetLogoutDataHandler(context: Activity) : RetrofitResponseHandler(context) {

        override fun onStart() {

        }

        override fun onSuccess(content: String) {
            dismissDialog()
            try {
                val response = XmlToJson.Builder(content).build()
                Debug.e("getLogoutData  #", response.toFormattedString())
                //var personMap: Map<String, Any> = gson.fromJson(json, object : TypeToken<Map<String, Any>>() {}.type)

                if (content != null) {
                    val res = Gson().fromJson<LoginItem>(response.toString(), object : TypeToken<LoginItem>() {}.type)

                    if (res.message.status == "success") {
                        showToast(res.message.text)

                        Global.clearLoginCredetials(getActivity())
                        initDrawerMenu(true)
                    } else {
                        showToast(res.message.text)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onFinish() {
            dismissDialog()
        }

        override fun onFailure(e: Throwable, content: String) {
            dismissDialog()
        }
    }
    //endregion

//    fun initBack(b: Boolean) {
//        val imgBack = findViewById<View>(R.id.imgBack) as ImageView
//        if (b) {
//            imgBack.visibility = View.VISIBLE
//            imgBack.setOnClickListener { finish() }
//        } else {
//            imgBack.visibility = View.GONE
//        }
//    }

    fun showSettingAlert() {
        val alertDialog = AlertDialog.Builder(getActivity())
        alertDialog.setTitle("GPS")
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?")
        alertDialog.setPositiveButton("Settings") { dialog, which ->
            dialog.dismiss()
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        }
        alertDialog.setNegativeButton("Cancel") { dialog, which ->
            dialog.dismiss()
            finish()
        }
        alertDialog.show()
    }

    fun getDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): String {
        val theta = lon1 - lon2
        var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + (Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta)))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist = dist * 60.0 * 1.1515
        return formatDeciPoint(dist)
    }

    fun formatDeciPoint(value: Double): String {
        val formatVal = DecimalFormat("##.##")
        return formatVal.format(value)
    }

    fun hideKeyboard() {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
        }

    }

    fun checkConnection(): Boolean {
        val info = getNetworkInfo(this)
        return info != null && info!!.isConnected()
    }

    fun getNetworkInfo(context: Context): NetworkInfo {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo
    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }

    fun takePicture() {
        initState()
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera()
        } else {
            if (!marshMallowPermission.checkPermissionForWriteexternal()) {
                marshMallowPermission.requestPermissionForWriteexternal()
            } else {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                try {
                    val state = Environment.getExternalStorageState()

                    if (Environment.MEDIA_MOUNTED == state) {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            mImageCaptureUri = FileProvider.getUriForFile(
                                this, BuildConfig.APPLICATION_ID + ".provider",
                                mFileTemp
                            )
                        } else {
                            mImageCaptureUri = Uri.fromFile(mFileTemp)
                        }
                    } else {
                        mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI
                    }

                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri)
                    intent.putExtra("return-data", true)
                    startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)

                } catch (e: ActivityNotFoundException) {

                }
            }
        }
    }

    fun openGallery() {
        if (!marshMallowPermission.checkPermissionForReadexternal()) {
            marshMallowPermission.requestPermissionForReadexternal()
        } else {
            val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image/*"
            startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY)
        }
    }

    fun getCurrentFragment(containerId: Int): Fragment? {
        return supportFragmentManager.findFragmentById(containerId)
    }

    fun initState() {
        val state = Environment.getExternalStorageState()
        if (Environment.MEDIA_MOUNTED == state) {
            mFileTemp = File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME)
        } else {
            mFileTemp = File(filesDir, TEMP_PHOTO_FILE_NAME)
        }
    }

    //region FOR GET CURRENT FRAGMENT OF VIEW-PAGER
//        (getCurrentFragmentOfViewPager(R.id.viewPager, viewPager) as AreYouFragment).setData()
    fun getCurrentFragmentOfViewPager(pagerId: Int, pager: ViewPager): Fragment? {
        return supportFragmentManager.findFragmentByTag("android:switcher:" + pagerId + ":" + pager.currentItem)
    }
    //endregion

    //region FOR SHOW SPINNER SELECTION
    fun showSpinnerSel(
        title: String,
        tv: TextView,
        data: ArrayList<Spinner>,
        isFilterable: Boolean,
        callback: SpinnerCallback?
    ) {
        val a = Dialog(getActivity())
        val w = a.window
        a.requestWindowFeature(Window.FEATURE_NO_TITLE)
        a.setContentView(R.layout.spinner_sel)
        w.setBackgroundDrawableResource(android.R.color.transparent)

        val lblselect = w.findViewById(R.id.dialogtitle) as TextView
        lblselect.text = title
        lblselect.text = title.replace("*", "").trim { it <= ' ' }

        //        TextView dialogClear = (TextView) w.findViewById(R.id.dialogClear);
        //        dialogClear.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View view) {
        //                tv.setText("");
        //                tv.setTag(null);
        //
        //                a.dismiss();
        //            }
        //        });

        val editSearch = w.findViewById<View>(R.id.editSearch) as EditText
        if (isFilterable) {
            editSearch.visibility = View.VISIBLE
        } else {
            editSearch.visibility = View.GONE
        }

        //        EditText editSearch = (EditText) w.findViewById(R.id.editSearch);
        //        if (isFilterable) {
        //            editSearch.setVisibility(View.VISIBLE);
        //        } else {
        //            editSearch.setVisibility(View.GONE);
        //        }

        val selectedStr = ""

        //        if (tv == editIntensity) {
        //            selectedStr = stoneParam.FancycolorIntensityList;
        //        }
        //        if (tv == editColor) {
        //            selectedStr = stoneParam.FancyColorList;
        //        }
        //        if (tv == editOvertone) {
        //            selectedStr = stoneParam.FancycolorOvertoneList;
        //        }


        if (!selectedStr.isEmpty()) {
            val selected = Global.asList(selectedStr.replace("'".toRegex(), ""))

            if (!selected.isEmpty()) {
                for (i in data.indices) {
                    if (selected.contains(data[i].ID)) {
                        data[i].setSelected(true)
                    } else {
                        data[i].setSelected(false)
                    }
                }
            }
        }


        val adapter = SpinnerSelAdapter(getActivity())
        adapter.setFilterable(isFilterable)
        val lv = w.findViewById(R.id.lvSpinner) as ListView
        lv.adapter = adapter
        adapter.addAll(data)

        //        if (tv.getTag().toString().trim()!= null){
        //            for (int i = 0; i <data.size() ; i++) {
        //                if (tv.getTag().toString().trim().equals(data.get(i).ID)){
        //                    Debug.e("Game Id" , data.get(i).ID);
        //                }
        //            }
        //        }

        lv.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                adapterview: AdapterView<*>, view: View,
                position: Int, l: Long
            ) {
                adapter.changeSelection(position, true)
            }
        }

        //        for (int i = 0; i <data.size() ; i++) {
        //            if (adapter.isSelectedAtleastOne()){
        //                adapter.changeSelection(Integer.valueOf(data.get(i).ID), true);
        //            }else{
        //                adapter.changeSelection(Integer.valueOf(data.get(i).ID), false);
        //            }
        //        }

        editSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.toString().trim { it <= ' ' }.length >= 1) {
                    adapter.filter!!.filter(editable.toString().trim { it <= ' ' })
                } else {
                    adapter.filter!!.filter("")
                }
            }
        })

        //        editSearch.addTextChangedListener(new TextWatcher() {
        //            @Override
        //            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //
        //            }
        //
        //            @Override
        //            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //
        //            }
        //
        //            @Override
        //            public void afterTextChanged(Editable editable) {
        //                if (editable.toString().trim().length() >= 1) {
        //                    adapter.getFilter().item_filter(editable.toString().trim());
        //                } else {
        //                    adapter.getFilter().item_filter("");
        //                }
        //
        //            }
        //        });

        val btnSpinnerOk = w.findViewById(R.id.btnPositive) as Button
        //        final View llGame = (View) w.findViewById(R.id.llGame);
        //        final TextView tvGameName = (TextView) w.findViewById(R.id.tvGameName);
        btnSpinnerOk.setOnClickListener(
            View.OnClickListener {
                val selList = adapter.selectedAll
                tv.setText(adapter.selectedTitle)
                tv.tag = adapter.selectedId

                if (selList.size > 0) {
                    tv.tag = adapter.selectedIdArray

                } else {
                    tv.tag = null
                }

                callback?.onDone(selList)

                a.dismiss()
            }

        )

        val btnSpinnerCancel = w
            .findViewById(R.id.btnNegative) as Button
        btnSpinnerCancel.setOnClickListener(
            View.OnClickListener { a.dismiss() }
        )
        a.show()
    }
    //endregion

    //region FOR SET TITLE TEXT
    fun setTitleText(text: String) {
        try {
            tvTitleText.text = Global.nullSafe(text, resources.getString(R.string.app_name))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    //endregion

    //region FOR INIT BACK PRESS
    fun initBackPress(b: Boolean) {
        if (b) {
            ivBack.visibility = View.VISIBLE
            ivBack.setOnClickListener { finish() }
        } else {
            ivBack.visibility = View.GONE
        }
    }
    //endregion

    //region FOR HIDE DRAWER MENU
    fun hideDrawerMenu(b: Boolean) {
        try {
            if (b)
                result.closeDrawer()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    //endregion

    //region FOR INIT DRAWER MENU
    fun initDrawerMenu(b: Boolean) {
        setSupportActionBar(toolbar)

        //FOR HEADER
        val inflaterHeader = LayoutInflater.from(getActivity())
        val headerView = inflaterHeader.inflate(R.layout.drawer_menu_header_main, null, false)
        headerView.tvUserName.text = "VAAPGLKNS"
        headerView.tvUserEmail.text = "ajay.eih@gmail.com"


        //FOR FOOTER
        val inflaterFooter = LayoutInflater.from(getActivity())
        val footerView = inflaterFooter.inflate(R.layout.drawer_menu_footer_main, null, false)
        footerView.tvVersionName.text = "Version 1.0.0"


        if (b) {
            ivMenu.visibility = View.VISIBLE

            result = DrawerBuilder()
                .withDrawerGravity(Gravity.START)
                .withActivity(this).withCloseOnClick(true).withSelectedItemByPosition(-1)
//                .withDrawerWidthDp(resources.getDimension(R.dimen._250sdp).toInt())
                .withSliderBackgroundColor(resources.getColor(R.color.white))
                .withHeader(headerView)
                .withFooter(footerView)
                .addDrawerItems(
                    PrimaryDrawerItem().withName("Shop by Category").withSelectable(false).withIcon(R.drawable.ic_home).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(0L),
                    PrimaryDrawerItem().withName("Login").withSelectable(false).withIcon(R.drawable.ic_login).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(1L),
                    PrimaryDrawerItem().withName("Register").withSelectable(false).withIcon(R.drawable.ic_register).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(2L),
                    PrimaryDrawerItem().withName("Men").withSelectable(false).withIcon(R.drawable.ic_men).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(3L),
                    PrimaryDrawerItem().withName("Women").withSelectable(false).withIcon(R.drawable.ic_women).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(4L),
                    PrimaryDrawerItem().withName("Cosmetic").withSelectable(false).withIcon(R.drawable.ic_cosmetic).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(5L),
                    PrimaryDrawerItem().withName("My WishList").withSelectable(false).withIcon(R.drawable.ic_white_list).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(6L),
                    PrimaryDrawerItem().withName("My Address").withSelectable(false).withIcon(R.drawable.ic_my_address).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(7L),
                    PrimaryDrawerItem().withName("My Orders").withSelectable(false).withIcon(R.drawable.ic_my_orders).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(8L),
                    PrimaryDrawerItem().withName("Checkout").withSelectable(false).withIcon(R.drawable.ic_checkout).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(9L),
                    PrimaryDrawerItem().withName("FAQ").withSelectable(false).withIcon(R.drawable.ic_faq).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(10L),
                    PrimaryDrawerItem().withName("Contact Us").withSelectable(false).withIcon(R.drawable.ic_contact_us).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(11L),
                    PrimaryDrawerItem().withName("Logout").withSelectable(false).withIcon(R.drawable.ic_logout).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(12L),
                    PrimaryDrawerItem().withName("Rate RedBlue App").withSelectable(false).withIcon(R.drawable.ic_rate_app).withTextColor(
                        getActivity().resources.getColor(R.color.black)
                    ).withTypeface(Typefaces.regularFont(getActivity())).withIdentifier(13L)
                )

                .withOnDrawerItemClickListener(Drawer.OnDrawerItemClickListener { view, position, drawerItem ->
                    when (drawerItem.identifier) {
                        1L -> {
                            if (getActivity() is LoginActivity) {
                                hideDrawerMenu(true)
                            } else {
                                val intent = Intent(getActivity(), LoginActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                                startNewActivity(intent)
                                hideDrawerMenu(true)
                                finishActivity()
                            }
                        }

                        2L -> {
                            if (getActivity() is RegisterActivity) {
                                hideDrawerMenu(true)
                            } else {
                                val intent = Intent(getActivity(), RegisterActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                                startNewActivity(intent)
                                hideDrawerMenu(true)
                                finishActivity()
                            }
                        }
                    }
                    true
                })
                .build()
            ivMenu.setOnClickListener {
                if (result.isDrawerOpen) {
                    result.closeDrawer()
                } else {
                    result.openDrawer()
                }
            }

            if (Global.isUserLoggedIn(getActivity())) {
                result.removeItem(1L)
                result.removeItem(2L)
            } else {
                result.removeItem(12L)
            }
        } else {
            ivMenu.visibility = View.GONE
        }
    }
    //endregion

    override fun onBackPressed() {
        try {
            if (result.isDrawerOpen) {
                result.closeDrawer()
            } else {
                super.onBackPressed()
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            }
        } catch (e: Exception) {
            super.onBackPressed()
        }
    }
}