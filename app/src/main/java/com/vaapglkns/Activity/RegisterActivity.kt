package com.vaapglkns.Activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vaapglkns.Global.*
import com.vaapglkns.Global.Debug
import com.vaapglkns.Global.Global
import com.vaapglkns.Global.RequestParamsUtils
import com.vaapglkns.Global.RetrofitHttpRequest
import com.vaapglkns.Model.RegisterItem
import com.vaapglkns.R
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject
import java.util.HashMap

class RegisterActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initDrawerMenu(true)
        initBackPress(false)
        initializeWidget()
        setWidgetOperations()

        if (Debug.DEBUG) {
            edEmail.setText("ajay.eih@gmail.com")
            edPassword.setText("123456")
            edConfirmPassword.setText("123456")
            edFirstName.setText("Ajay")
            edLastName.setText("Sojitra")
        }
    }

    //region FOR INITIALIZE WIDGET
    private fun initializeWidget() {
        setTitleText("Register Account")
    }
    //endregion

    //region FOR SET WIDGET OPERATIONS
    private fun setWidgetOperations() {
        btnRegister.setOnClickListener(this)
    }
    //endregion

    //region FOR VALIDATE REGISTER DATA
    private fun validateRegisterData(
        firstName: String,
        lastName: String,
        email: String,
        password: String,
        confirmPassword: String,
        isSubscribed: Boolean
    ): Boolean {

        if (!TextUtils.isEmpty(firstName)) {
            if (!TextUtils.isEmpty(lastName)) {
                if (!TextUtils.isEmpty(email)) {
                    if (Global.isValidEmail(email)) {
                        if (!TextUtils.isEmpty(password)) {
                            if (password.length >= 6) {
                                if (!TextUtils.isEmpty(confirmPassword)) {
                                    if (confirmPassword.length >= 6) {
                                        if (confirmPassword == password) {
                                            if (isSubscribed) {
                                                return true
                                            } else {
                                                showToast("You must have to Subscribe for Register!")
                                            }
                                        } else {
                                            showToast("Password and Confirm-Password must be same!")
                                        }
                                    } else {
                                        showToast("Please enter valid Confirm-Password!")
                                    }
                                } else {
                                    showToast("Please enter your Confirm-Password!")
                                }
                            } else {
                                showToast("Please enter valid Password!")
                            }
                        } else {
                            showToast("Please enter your Password!")
                        }
                    } else {
                        showToast("Please enter valid Email-Id!")
                    }
                } else {
                    showToast("Please enter your Email-Id!")
                }
            } else {
                showToast("Please enter your Last Name!")
            }
        } else {
            showToast("Please enter your First Name!")
        }

        return false
    }
    //endregion

    //region CALL SERVICE FOR REGISTER USER
    private fun registerUser(firstName: String, lastName: String, email: String, password: String) {
        try {
            showDialog("")

            val bodyParameter = HashMap<String, String>()
            bodyParameter[RequestParamsUtils.FIRSTNAME] = firstName
            bodyParameter[RequestParamsUtils.LASTNAME] = lastName
            bodyParameter[RequestParamsUtils.EMAIL] = email
            bodyParameter[RequestParamsUtils.PASSWORD] = password
            bodyParameter[RequestParamsUtils.CONFIRMATION] = password
            bodyParameter[RequestParamsUtils.IS_SUBSCRIBED] = "true"
            bodyParameter[RequestParamsUtils.DEVICE_TOKEN] = "vaapglkns88888888"
            bodyParameter[RequestParamsUtils.DEVICE_TYPE] = "1"
            bodyParameter[RequestParamsUtils.DEVICE_ID] = Global.getDeviceId(getActivity())

            Debug.e("Parameters: ", JSONObject(bodyParameter).toString())
            val retrofit = RetrofitHttpRequest.newRequestRetrofit(getActivity())
            val retrofitService = retrofit.create(RetrofitUrls::class.java)
            retrofitService.register(bodyParameter).enqueue(GetRegisterDataHandler(getActivity()))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class GetRegisterDataHandler(context: Activity) : RetrofitResponseHandler(context) {

        override fun onStart() {

        }

        override fun onSuccess(content: String) {
            dismissDialog()
            try {
                val response = XmlToJson.Builder(content).build()
                Debug.e("getRegisterData  #", response.toFormattedString())

                if (content != null) {
                    val res = Gson().fromJson<RegisterItem>(JSONObject(response.toFormattedString()).toString(), object : TypeToken<RegisterItem>() {}.type)

                    if (res.message.status == "success") {
                        showToast(res.message.text)

//                        startNewActivity(Intent(getActivity(), LoginActivity::class.java))
//                        finishAffinity()

                        //FOR SAVE USER TOKEN AND USE IN SHARED PREFERENCE
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_TOKEN, "vaapglkns")
                        Global.setPref(getActivity(), StaticDataUtility.sUSER_ID, "vaapglkns")

//                        startNewActivity(Intent(getActivity(), MainActivity::class.java))
//                        finishAffinity()
                    } else {
                        showToast(res.message.text)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onFinish() {
            dismissDialog()
        }

        override fun onFailure(e: Throwable, content: String) {
            dismissDialog()
        }
    }
    //endregion

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnRegister -> {
                val firstName = edFirstName.text.toString()
                val lastName = edLastName.text.toString()
                val email = edEmail.text.toString()
                val password = edPassword.text.toString()
                val confirmPassword = edConfirmPassword.text.toString()
                val isSubscribed = swSubscribe.isChecked
                if (validateRegisterData(firstName, lastName, email, password, confirmPassword, isSubscribed)) {
                    registerUser(firstName, lastName, email, password)
                }
            }
        }
    }
}
