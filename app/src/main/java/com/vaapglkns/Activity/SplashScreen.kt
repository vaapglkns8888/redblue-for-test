package com.vaapglkns.Activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import com.vaapglkns.Global.Debug
import com.vaapglkns.Global.Global
import com.vaapglkns.Global.StaticDataUtility
import com.vaapglkns.R
import kotlinx.android.synthetic.main.row_forgot_password.view.*
import com.vaapglkns.Global.GpsUtils

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
open class SplashScreen : BaseActivity() {
    internal var TAG = SplashScreen::class.java.simpleName
    internal var handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
//        if (Global.isInternetConnected(getActivity())) {
//            checkGPSLocation()
//        } else {
//            handler.post(mPostInternetConDialog)
//        }

        checkRequiredPermission()

        //FOR CHECK IF NAVIGATION BAR
        if (hasNavBar(windowManager)) {
            window.decorView.apply {
                // Hide both the navigation bar and the status bar.
                // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
                // a general rule, you should design your app to hide the status bar whenever you
                // hide the navigation bar.
                systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
            }
//        } else {
//            llNavigation.visibility = View.GONE
        }
    }

    //region FOR CHECK GPS IS ON OR OFF
    private fun checkGPSLocation() {
        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gps_enabled = false
        var network_enabled = false
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        if (!gps_enabled && !network_enabled) {
            val dialog = AlertDialog.Builder(getActivity())
            dialog.setMessage("GPS network not enabled..!")
            dialog.setPositiveButton("Turn GPS ON") { paramDialogInterface, paramInt ->
                GpsUtils(this).turnGPSOn { isGPSEnable ->

                }

//                startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), StaticDataUtility.REQUEST_GPS)
            }
            dialog.setNegativeButton("Cancel") { paramDialogInterface, paramInt ->
            }
            dialog.show()
        } else {
            checkRequiredPermission()
        }
    }
    //endregion

    //region FOR CHECK REQUIRED PERMISSION
    private fun checkRequiredPermission() {
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        if (Global.hasPermissions(getActivity(), permissions)) {
            startApplication(1000)
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val remainingPermissions = ArrayList<String>()
                for (permission in permissions) {
                    if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                        remainingPermissions.add(permission)
                    }
                }
                requestPermissions(remainingPermissions.toTypedArray(), StaticDataUtility.REQUEST_PERMISSION)
            }
        }
    }
    //endregion

    private fun startApplication(sleepTime: Long) {
        handler.postDelayed(startApp, sleepTime)
    }

    private var startApp: Runnable = object : Runnable {
        override fun run() {
            handler.removeCallbacks(this)
            Debug.e(TAG, "startApp")
//            if (!Global.isUserLoggedIn(getActivity())) {
//                val i = Intent(getActivity(), LoginActivity::class.java)
//                startNewActivity(i)
//                finish()
//            } else {
//                val i = Intent(getActivity(), MainActivity::class.java)
//                startNewActivity(i)
//                finish()
//            }
            val i = Intent(getActivity(), LoginActivity::class.java)
            startNewActivity(i)
            finish()
        }
    }

    internal var count = 30
    internal var mPostInternetConDialog: Runnable = Runnable {
        val builder = AlertDialog.Builder(getActivity())
            .setTitle(getActivity().resources.getString(R.string.connection_title))
            .setMessage(getActivity().resources.getString(R.string.connection_not_available))
            .setPositiveButton(getActivity().resources.getString(R.string.btn_enable), DialogInterface.OnClickListener { dialog, which ->
                try {
                    val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                    startNewActivity(intent)
                    finish()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
            .setNegativeButton(getActivity().resources.getString(R.string.btn_cancel), DialogInterface.OnClickListener { dialog, which ->
                finish()
            })
        val dialog = builder.create()
        dialog.show()
    }

    internal var postLocationDialog: Runnable = Runnable {
        val builder = AlertDialog.Builder(getActivity())
            .setTitle(getActivity().resources.getString(R.string.location_title))
            .setMessage(getActivity().resources.getString(R.string.location_msg))
            .setPositiveButton(getActivity().resources.getString(R.string.btn_settings), DialogInterface.OnClickListener { dialog, which ->
                try {
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startNewActivity(intent)
                    finish()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
            .setNegativeButton(getActivity().resources.getString(R.string.btn_cancel), DialogInterface.OnClickListener { dialog, which ->  })

        val dialog = builder.create()
        dialog.show()
    }

    private var checkConnection: Runnable = object : Runnable {
        override fun run() {
            Debug.e(TAG, "checkConnection")
            if (Global.isInternetConnected(getActivity())) {
                handler.removeCallbacks(this)
                if (Global.isInternetConnected(getActivity())) {
                    startApplication(1000)
                } else {
                    handler.post(mPostInternetConDialog)
                }
            } else {
                if (count != 0) {
                    handler.postDelayed(this, 1000)
                } else {
                    finish()
                }
            }
        }
    }

    //region FOR SHOW CUSTOM LAYOUT POPUP
    private fun showCustomLayoutPopup() {
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.row_forgot_password, null)
        //INITIALIZE WIDGET


        val builder = AlertDialog.Builder(getActivity())
            .setCancelable(true)
            .setTitle(getActivity().resources.getString(R.string.forgot_password))
        val dialog = builder.create()
        dialog.setView(alertLayout)

        alertLayout.tvRowReset.setOnClickListener {
            val email = alertLayout.edRowEmail.text.toString()
            if (!TextUtils.isEmpty(email)) {
                if (Global.isValidEmail(email)) {
//                    forgotPassword(email)
                    dialog.dismiss()
                } else {
                    showToast("Please enter valid Email-id.!")
                }
            } else {
                showToast("Please enter your Email-id.!")
            }
        }

        alertLayout.tvRowCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
    //endregion

    //region CHECK FOR HAS NAVIGATION BAR
    private fun hasNavBar(windowManager: WindowManager): Boolean {
        val d = windowManager.defaultDisplay
        val realDisplayMetrics = DisplayMetrics()
        d.getRealMetrics(realDisplayMetrics)
        val realHeight = realDisplayMetrics.heightPixels
        val realWidth = realDisplayMetrics.widthPixels
        val displayMetrics = DisplayMetrics()
        d.getMetrics(displayMetrics)
        val displayHeight = displayMetrics.heightPixels
        val displayWidth = displayMetrics.widthPixels
        return realWidth - displayWidth > 0 || realHeight - displayHeight > 0
    }
    //endregion

    fun versionUpgradeDialog() {
        val builder = AlertDialog.Builder(getActivity())
            .setTitle("Update")
            .setMessage(R.string.update_text)
            .setPositiveButton("Update", DialogInterface.OnClickListener { dialog, which ->
                try {
                    val appPackageName = packageName
                    try {
                        startNewActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                    } catch (anfe: android.content.ActivityNotFoundException) {
                        startNewActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("http://play.google.com/store/apps/details?id=$appPackageName")
                            )
                        )
                    }
                    finish()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
            .setNegativeButton(R.string.btn_cancel, DialogInterface.OnClickListener { dialog, which ->
                finish()
            })
        val dialog = builder.create()
        dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            StaticDataUtility.REQ_CODE_SETTING -> {
                handler.post(checkConnection)
            }

            StaticDataUtility.REQUEST_GPS -> {
                if (resultCode == Activity.RESULT_OK) {
                    checkGPSLocation()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        var isPermissionGranted = true
        if (requestCode == StaticDataUtility.REQUEST_PERMISSION) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val remainingPermissions = ArrayList<String>()
                for (permission in permissions) {
                    if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                        remainingPermissions.add(permission)
                        isPermissionGranted = false
                    }
                }
                if (isPermissionGranted) {
                    startApplication(1000)
                } else {
                    requestPermissions(remainingPermissions.toTypedArray(), StaticDataUtility.REQUEST_PERMISSION)
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            handler.removeCallbacks(startApp)
            handler.removeCallbacks(checkConnection)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}