package com.vaapglkns.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vaapglkns.Model.MyEventRes
import com.vaapglkns.Global.Debug
import com.vaapglkns.Global.Global
import com.vaapglkns.R
import kotlinx.android.synthetic.main.row_category.view.*
import java.util.*

/**
 * Created by xpro on 1/30/2017.
 */

class MyEventListAdapter(internal var context: Context) : RecyclerView.Adapter<MyEventListAdapter.MyViewHolder>(), Filterable {

    private var data: MutableList<MyEventRes.Datum>? = ArrayList<MyEventRes.Datum>()

    internal var myEventAdapter: MyEventListAdapter? = null
    private val dataSource = ArrayList<MyEventRes.Datum>()
    private lateinit var mEventListener: EventListener
    internal var isFilterable = false

    fun addAll(mData: List<MyEventRes.Datum>) {
//        imageLoader = Utils.initImageLoader(context)!!

        try {
            this.data!!.clear()
            this.data!!.addAll(mData)

            if (isFilterable) {
                this.dataSource.clear()
                this.dataSource.addAll(mData)
            }

        } catch (e: Exception) {
            e.printStackTrace()
            //Utils.sendExceptionReport(e);
        }
        //        data.clear();
        //        data.addAll(mData);
        notifyDataSetChanged()
    }

    fun add(mData: MyEventRes.Datum) {
        data!!.add(mData)
        notifyDataSetChanged()
    }

    fun clear() {
        data!!.clear()
        //        mdata.clear();
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.row_category, parent, false)
        return MyViewHolder(v)
    }

    //    public void remove(int position, int id) {
    //        mData = mData + "," + id;
    //        data.remove(position);
    //        notifyDataSetChanged();
    //    }

    fun getItem(pos: Int): MyEventRes.Datum {
        return data!![pos]
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = data!![position]

        if (!item.featuredImage.isNullOrEmpty()) {
            Glide.with(context).load(Global.nullSafe(item.featuredImage)).into(holder.imgEvent)
        } else {
            holder.imgEvent.setImageDrawable(context.resources.getDrawable(R.drawable.ic_launcher_background))
        }

        holder.tvInvitationTitle!!.setText(item.eventName)
        holder.container!!.setOnClickListener {
            if (mEventListener != null) {
                Toast.makeText(context, "Item Clicked" + position.toString(), Toast.LENGTH_SHORT).show()
                mEventListener.onItemViewClicked(position)
            }
        }
    }

    override fun getItemCount(): Int {

        return data!!.size
        //        return 3;
    }

    fun setFilterable(isFilterable: Boolean) {
        this.isFilterable = isFilterable
    }

    fun setEventListener(eventListener: EventListener) {
        mEventListener = eventListener
    }

    override fun getFilter(): Filter? {
        return if (isFilterable) {
            PTypeFilter()
        } else null
    }

    interface EventListener {

        fun onItemViewClicked(position: Int)
    }

    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        val imgEvent = v.imgInvitation
        val container = v.container
        val tvInvitationTitle = v.tvInvitationTitle
    }

    private inner class PTypeFilter : Filter() {
        override fun publishResults(prefix: CharSequence, results: Filter.FilterResults) {
            // NOTE: this function is *always* called from the UI thread.
            data = results.values as MutableList<MyEventRes.Datum>
            if (data != null) {
                notifyDataSetChanged()
            } else {
                data = dataSource
                notifyDataSetChanged()
            }
        }

        override fun performFiltering(prefix: CharSequence?): Filter.FilterResults {
            // NOTE: this function is *always* called from a background thread,
            // and
            // not the UI thread.

            val results = Filter.FilterResults()
            val new_res = ArrayList<MyEventRes.Datum>()
            if (prefix != null && prefix.toString().length > 0) {
                Debug.e("data sourch size", "" + dataSource.size)
                for (index in dataSource.indices) {
                    try {
                        val si = dataSource[index]

                        if (si.eventName!!.toLowerCase().contains(prefix.toString().toLowerCase()) ||
                                si.city!!.toLowerCase().contains(prefix.toString().toLowerCase()) ||
                                si.state!!.toLowerCase().contains(prefix.toString().toLowerCase()) ||
                                si.country!!.toLowerCase().contains(prefix.toString().toLowerCase())) {
                            new_res.add(si)
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
                results.values = new_res
                results.count = new_res.size

            } else {
                Debug.e("", "Called synchronized view")

                results.values = dataSource
                results.count = dataSource.size

            }

            return results
        }
    }

}
