package com.vaapglkns.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vaapglkns.Model.Movie
import com.vaapglkns.Global.Global
import com.vaapglkns.R
import kotlinx.android.synthetic.main.item_movie_list.view.*
import java.util.*

/**
 * Created by VAAPGLKNS on 1/30/2017.
 */

class MovielistAdapter(internal var context: Context) : RecyclerView.Adapter<MovielistAdapter.MyViewHolder>() {

    private var data: MutableList<Movie.Result>? = ArrayList<Movie.Result>()
    internal var myEventAdapter: MovielistAdapter? = null
    private lateinit var mEventListener: EventListener

    fun add(mData: Movie.Result) {
        data!!.add(mData)
        notifyDataSetChanged()
    }

    fun addAll(mData: List<Movie.Result>) {
        clear()
        try {
            this.data!!.addAll(mData)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        notifyDataSetChanged()
    }

    fun clear() {
        data!!.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.item_movie_list, parent, false)
        return MyViewHolder(v)
    }

    fun getItem(pos: Int): Movie.Result {
        return data!![pos]
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = data!![position]

        if (!item.posterPath.isNullOrEmpty()) {
            Glide.with(context).load(Global.nullSafe("http://image.tmdb.org/t/p/original/" + item.posterPath))
                .placeholder(R.drawable.ic_event_img)
                .into(holder.imgEvent)
        }

        holder.tvInvitationTitle!!.setText(item.title)
        holder.container!!.setOnClickListener {
            if (mEventListener != null) {
                mEventListener.onItemViewClicked(position)
            }
        }
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    fun setEventListener(eventListener: EventListener) {
        mEventListener = eventListener
    }

    interface EventListener {
        fun onItemViewClicked(position: Int)
    }

    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val imgEvent = v.imgMovie
        val container = v.container
        val tvInvitationTitle = v.tvMovieTitle
    }
}
