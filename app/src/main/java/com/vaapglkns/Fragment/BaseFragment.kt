package com.vaapglkns.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.NavUtils
import com.vaapglkns.Global.RetrofitProgressDialog

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
open class BaseFragment : Fragment() {
    internal lateinit var toast: Toast
    internal var ad: RetrofitProgressDialog? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toast = Toast.makeText(activity, "", Toast.LENGTH_LONG)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item != null) {
            when (item.getItemId()) {
                android.R.id.home -> {
                    NavUtils.navigateUpFromSameTask(activity!!)
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun showToast(text: String, duration: Int) {
        activity!!.runOnUiThread {
            toast.setText(text)
            toast.duration = duration
            toast.show()
        }
    }

    fun showDialog(msg: String) {
        try {
            if (ad != null && ad!!.isShowing) {
                return
            }
            ad = RetrofitProgressDialog.getInstant(activity!!)
            ad!!.show(msg)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setMessage(msg: String) {
        try {
            ad!!.setMessage(msg)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun dismissDialog() {
        try {
            if (ad != null) {
                ad!!.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
