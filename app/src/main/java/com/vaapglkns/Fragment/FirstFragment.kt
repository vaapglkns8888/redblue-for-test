package com.vaapglkns.Fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.vaapglkns.Model.Movie
import com.vaapglkns.Adapter.MovielistAdapter
import com.vaapglkns.R
import kotlinx.android.synthetic.main.first_fragment_fragment.*

/**
 * Created by VAAPGLKNS on 13-Jun-19.
 */
class FirstFragment : BaseFragment() {
    private var type: Int = 0
    private lateinit var movielistAdapter: MovielistAdapter

    companion object {
        private val LAYOUT_ID = "layoutid"
        fun newInstance(layoutId: Int, type: Int): FirstFragment {
            val pane = FirstFragment()
            val args = Bundle()
            args.putInt(LAYOUT_ID, layoutId)
            args.putInt("type", type)
            pane.arguments = args
            return pane
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.first_fragment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        type = arguments!!.getInt("type", 0)
        init()
    }

    private fun init() {
        recyclerViewFragment!!.layoutManager = LinearLayoutManager(activity)
        movielistAdapter = MovielistAdapter(this.activity!!)
        recyclerViewFragment!!.adapter = movielistAdapter

        movielistAdapter.setEventListener(object : MovielistAdapter.EventListener {
            override fun onItemViewClicked(position: Int) {
                val data = movielistAdapter.getItem(position)
//                val intent = Intent(activity, MovieDetailActivity::class.java)
//                intent.putExtra("movieId", data.id)
//                startActivity(intent)
            }
        })
    }

    fun fillData(res: Movie?) {
        if (res != null) {
//            if (type == FragmentActivity.TYPE_PAST) {
//                recyclerViewFragment!!.layoutManager = LinearLayoutManager(activity)
//                movielistAdapter.addAll(res.results)
//            } else if (type == FragmentActivity.TYPE_FUTURE) {
//                recyclerViewFragment!!.layoutManager = GridLayoutManager(activity, 2)
//                movielistAdapter.addAll(res.results)
//            }
        }
    }
}