package com.vaapglkns.Global

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.*
import android.content.*
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.content.res.Configuration
import android.database.Cursor
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.VectorDrawable
import android.location.LocationManager
import android.media.MediaScannerConnection
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.CountDownTimer
import android.os.Environment
import android.preference.PreferenceManager
import android.provider.ContactsContract
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import android.telephony.TelephonyManager
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ImageSpan
import android.util.*
import android.util.Base64
import android.view.View
import android.view.animation.*
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.webkit.URLUtil
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import com.rey.material.app.Dialog
import com.rey.material.app.DialogFragment
import com.rey.material.app.SimpleDialog
import com.vaapglkns.BuildConfig
import com.vaapglkns.R
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.*
import java.lang.reflect.InvocationTargetException
import java.math.BigDecimal
import java.net.MalformedURLException
import java.net.URL
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.*
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.experimental.and

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
internal object Global {
    var TYPE_NOT_CONNECTED = 0
    lateinit var dialogListener: DialogClickListener
    var mDeleteFileCount: Long = 0

    //region FOR MANAGE PREFERENCE
    fun setPref(c: Context, pref: String, `val`: String) {
        val e = PreferenceManager.getDefaultSharedPreferences(c).edit()
        e.putString(pref, `val`)
        e.apply()
    }

    fun getPref(c: Context, pref: String, `val`: String): String? {
        return PreferenceManager.getDefaultSharedPreferences(c).getString(
            pref,
            `val`
        )
    }

    fun setPref(c: Context, pref: String, `val`: Boolean) {
        val e = PreferenceManager.getDefaultSharedPreferences(c).edit()
        e.putBoolean(pref, `val`)
        e.apply()
    }

    fun getPref(c: Context, pref: String, `val`: Boolean): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(c).getBoolean(
            pref, `val`
        )
    }

    fun delPref(c: Context, pref: String) {
        val e = PreferenceManager.getDefaultSharedPreferences(c).edit()
        e.remove(pref)
        e.apply()
    }

    fun setPref(c: Context, pref: String, `val`: Int) {
        val e = PreferenceManager.getDefaultSharedPreferences(c).edit()
        e.putInt(pref, `val`)
        e.apply()

    }

    fun getPref(c: Context, pref: String, `val`: Int): Int {
        return PreferenceManager.getDefaultSharedPreferences(c).getInt(
            pref,
            `val`
        )
    }

    fun setPref(c: Context, pref: String, `val`: Long) {
        val e = PreferenceManager.getDefaultSharedPreferences(c).edit()
        e.putLong(pref, `val`)
        e.apply()
    }

    fun getPref(c: Context, pref: String, `val`: Long): Long {
        return PreferenceManager.getDefaultSharedPreferences(c).getLong(
            pref,
            `val`
        )
    }

    fun setPref(c: Context, file: String, pref: String, `val`: String) {
        val settings = c.getSharedPreferences(
            file,
            Context.MODE_PRIVATE
        )
        val e = settings.edit()
        e.putString(pref, `val`)
        e.apply()

    }

    fun getPref(c: Context, file: String, pref: String, `val`: String): String? {
        return c.getSharedPreferences(file, Context.MODE_PRIVATE).getString(
            pref, `val`
        )
    }
    //endregion

    //region FOR VALIDATE EMAIL
    fun validateEmail(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                .matches()
        }
    }
    //endregion

    //region FOR VALIDATE DATA
    fun validate(target: String, pattern: String): Boolean {
        if (TextUtils.isEmpty(target)) {
            return false
        } else {
            val r = Pattern.compile(pattern)
            return r.matcher(target).matches()
        }
    }
    //endregion

    //region CHECK IS ALPHA NUMERIC
    fun isAlphaNumeric(target: String): Boolean {
        if (TextUtils.isEmpty(target)) {
            return false
        } else {
            val r = Pattern.compile("^[a-zA-Z0-9]*$")
            return r.matcher(target)
                .matches()
        }
    }
    //endregion

    //region CHECK FOR IS NUMERIC
    fun isNumeric(target: String): Boolean {
        if (TextUtils.isEmpty(target)) {
            return false
        } else {
            val r = Pattern.compile("^[0-9]*$")
            return r.matcher(target)
                .matches()
        }
    }
    //endregion

    //region FOR GET DEVICE WIDTH
    fun getDeviceWidth(context: Context): Int {
        try {
            val metrics = context.resources.displayMetrics
            return metrics.widthPixels
        } catch (e: Exception) {
            sendExceptionReport(e)
        }
        return 480
    }
    //endregion

    //region FOR GET DEVICE HEIGHT
    fun getDeviceHeight(context: Context): Int {
        try {
            val metrics = context.resources.displayMetrics
            return metrics.heightPixels
        } catch (e: Exception) {
            sendExceptionReport(e)
        }
        return 800
    }
    //endregion

    //region FOR GET DEVICE DENSITY
    fun getDeviceDensity(context: Context): Float {
        return context.resources.displayMetrics.density
    }
    //endregion

    //region CHECK FOR IS INTERNET CONNECTED
    fun isInternetConnected(mContext: Context?): Boolean {
        var outcome = false
        try {
            if (mContext != null) {
                val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = cm.allNetworkInfo

                for (tempNetworkInfo in networkInfo) {
                    if (tempNetworkInfo.isConnected) {
                        outcome = true
                        break
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return outcome
    }
    //endregion

    //region FOR GET DEVICE ID
    fun getDeviceId(c: Context): String {
        var aid: String?
        try {
            aid = ""
            aid = Settings.Secure.getString(c.contentResolver, "android_id")
            if (aid == null) {
                aid = "No DeviceId"
            } else if (aid.length <= 0) {
                aid = "No DeviceId"
            }
        } catch (e: Exception) {
            sendExceptionReport(e)
            aid = "No DeviceId"
        }
        return aid!!
    }
    //endregion

    //region FOR GET RANDOM FLOAT VALUE
    fun random(min: Float, max: Float): Float {
        return (min + Math.random() * (max - min + 1)).toFloat()
    }
    //endregion

    //region FOR GET RANDOM INT VALUE
    fun random(min: Int, max: Int): Int {
//        return Math.round((min + Math.random() * (max - min + 1)).toFloat())
        return Random().nextInt(max - min + 1) + min
    }
    //endregion

    //region FOR HAS FLASH FEATURE
    fun hasFlashFeature(context: Context): Boolean {
        return context.packageManager.hasSystemFeature(
            PackageManager.FEATURE_CAMERA_FLASH
        )
    }
    //endregion

    //region FOR HAS CAMERA FEATURES
    fun hasCameraFeature(context: Context): Boolean {
        return context.packageManager.hasSystemFeature(
            PackageManager.FEATURE_CAMERA
        )
    }
    //endregion

    //region FOR HIDE KEYBOARD
    fun hideKeyBoard(c: Context, v: View) {
        val imm = c
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)
    }
    //endregion

    // region FOR SHOW KEYBOARD
    fun showKeyBoard(c: Context, v: View) {
        val imm = c.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT)
    }
    //endregion

    //region FOR GET BOLD FONT TYPEFACES
    fun getBold(c: Context): Typeface? {
        try {
            return Typeface.createFromAsset(
                c.assets,
                "fonts/pt_sans_bold.ttf"
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }
    //endregion

    //region FOR GET NORMAL FONT TYPEFACES
    fun getNormal(c: Context): Typeface? {
        try {
            return Typeface.createFromAsset(
                c.assets,
                "fonts/Gabriela-Regular.ttf"
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }
    //endregion

    //region FOR VALIDATE PASSWORD
    fun passwordValidator(password: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        //        String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
        val PASSWORD_PATTERN = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,15}$"

        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }
    //endregion

    fun formatNo(str: String): String? {
        val number = removeComma(nullSafe(str))
        return if (!TextUtils.isEmpty(number)) {
            //            if (!finalStr.startsWith("$"))
            //                finalStr = "$" + finalStr;
            formatToComma(number)
        } else number
    }

    fun `formatNo$`(str: String): String {
        val number = removeComma(nullSafe(str))
        if (!TextUtils.isEmpty(number)) {
            var finalStr = formatToComma(number)
            if (!finalStr!!.startsWith("$"))
                finalStr = "$$finalStr"
            return finalStr
        }
        return number
    }

    fun formatToComma(str: String): String? {
        var number: String? = removeComma(nullSafe(str))
        if (!TextUtils.isEmpty(number)) {

            var finalStr: String
            if (number!!.contains(".")) {
                number = truncateUptoTwoDecimal(number)
                val decimalFormat = DecimalFormat("#.##")
                finalStr = decimalFormat.format(BigDecimal(number!!))
            } else {
                finalStr = number
            }
            finalStr = NumberFormat.getNumberInstance(Locale.US).format(java.lang.Double.valueOf(finalStr))
            return finalStr
        }
        return number
    }

    fun truncateUptoTwoDecimal(doubleValue: String): String? {
        if (doubleValue != null) {
            var result: String = doubleValue
            val decimalIndex = result.indexOf(".")
            if (decimalIndex != -1) {
                val decimalString = result.substring(decimalIndex + 1)
                if (decimalString.length > 2) {
                    result = doubleValue.substring(0, decimalIndex + 3)
                } else if (decimalString.length == 1) {
                    //                    result = String.format(Locale.ENGLISH, "%.2f",
                    //                            Double.parseDouble(value));
                }
            }
            return result
        }
        return doubleValue
    }

    fun removeComma(str: String): String {
        var str = str
        try {
            if (!TextUtils.isEmpty(str)) {
                str = str.replace(",".toRegex(), "")
                try {
                    val format = NumberFormat.getCurrencyInstance()
                    val number = format.parse(str)
                    return number.toString()
                } catch (e: ParseException) {
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        Debug.e("removeComma", "" + str)
        return str
    }

    fun getRowFadeSpeedAnimation(c: Context): LayoutAnimationController {
        val anim = AnimationUtils.loadAnimation(
            c,
            R.anim.raw_fade
        ) as AlphaAnimation
        return LayoutAnimationController(
            anim, 0.3f
        )
    }

    fun sendExceptionReport(e: Exception) {
        e.printStackTrace()

        try {
            // Writer result = new StringWriter();
            // PrintWriter printWriter = new PrintWriter(result);
            // e.printStackTrace(printWriter);
            // String stacktrace = result.toString();
            // new CustomExceptionHandler(c, URLs.URL_STACKTRACE)
            // .sendToServer(stacktrace);
        } catch (e1: Exception) {
            e1.printStackTrace()
        }

    }


    fun getAndroidId(c: Context): String {
        var aid: String?
        try {
            aid = ""
            aid = Settings.Secure.getString(
                c.contentResolver,
                "android_id"
            )

            if (aid == null) {
                aid = "No DeviceId"
            } else if (aid.length <= 0) {
                aid = "No DeviceId"
            }
        } catch (e: Exception) {
            e.printStackTrace()
            aid = "No DeviceId"
        }

        Debug.e("", "aid : " + aid!!)

        return aid

    }

    fun getAppVersionCode(c: Context): Int {
        try {
            return c.packageManager.getPackageInfo(c.packageName, 0).versionCode
        } catch (e: Exception) {
            sendExceptionReport(e)
        }
        return 0
    }

    fun getAppVersionName(c: Context): String {
        try {
            return c.packageManager.getPackageInfo(c.packageName, 0).versionName
        } catch (e: Exception) {
            sendExceptionReport(e)
        }
        return "1.0"
    }

    fun getPhoneModel(c: Context): String {

        try {
            return Build.MODEL
        } catch (e: Exception) {
            sendExceptionReport(e)
        }

        return ""
    }

    fun getPhoneBrand(c: Context): String {

        try {
            return Build.BRAND
        } catch (e: Exception) {
            sendExceptionReport(e)
        }

        return ""
    }

    fun getOsVersion(c: Context): String {

        try {
            return Build.VERSION.RELEASE
        } catch (e: Exception) {
            sendExceptionReport(e)
        }

        return ""
    }

    fun getOsAPILevel(c: Context): Int {

        try {
            return Build.VERSION.SDK_INT
        } catch (e: Exception) {
            sendExceptionReport(e)
        }

        return 0
    }

    fun parseCalendarFormat(c: Calendar, pattern: String): String {
        val sdf = SimpleDateFormat(
            pattern,
            Locale.getDefault()
        )
        return sdf.format(c.time)
    }

    fun parseTime(time: Long, pattern: String): String {
        val sdf = SimpleDateFormat(
            pattern,
            Locale.getDefault()
        )
        return sdf.format(Date(time))
    }

    fun parseTime(time: String, pattern: String): Date {
        val sdf = SimpleDateFormat(
            pattern,
            Locale.getDefault()
        )
        try {
            return sdf.parse(time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return Date()
    }

    fun parseTimeForDate(time: Long, pattern: String): Date {
        Debug.e("timezone", TimeZone.getDefault().id)
        val sdf = SimpleDateFormat(
            pattern,
            Locale.getDefault()
        )

        try {
            return sdf.parse(sdf.format(Date(time)))

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return Date()
    }


    fun parseTime(
        time: String, fromPattern: String,
        toPattern: String
    ): String {
        var sdf = SimpleDateFormat(
            fromPattern,
            Locale.getDefault()
        )
        try {
            val d = sdf.parse(time)
            sdf = SimpleDateFormat(toPattern, Locale.getDefault())
            return sdf.format(d)
        } catch (e: Exception) {
            Debug.i("parseTime", "" + e.message)
        }

        return ""
    }

    fun parseTimeUTCtoDefault(time: String, pattern: String): Date {
        var sdf = SimpleDateFormat(
            pattern,
            Locale.getDefault()
        )
        try {
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val d = sdf.parse(time)
            sdf = SimpleDateFormat(pattern, Locale.getDefault())
            sdf.timeZone = Calendar.getInstance().timeZone
            return sdf.parse(sdf.format(d))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return Date()
    }

    fun parseTimeUTCtoDefault(time: Long): Date {
        try {
            val cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            cal.timeInMillis = time
            val d = cal.time
            val sdf = SimpleDateFormat()
            sdf.timeZone = Calendar.getInstance().timeZone
            return sdf.parse(sdf.format(d))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return Date()
    }

    fun parseTimeUTCtoDefault(time: String, fromPattern: String, toPattern: String): String {
        var sdf = SimpleDateFormat(
            fromPattern,
            Locale.getDefault()
        )
        try {
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val d = sdf.parse(time)
            sdf = SimpleDateFormat(toPattern, Locale.getDefault())
            sdf.timeZone = Calendar.getInstance().timeZone
            return sdf.format(d)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    fun parseTimeUTCtoDefaultGerman(time: String, fromPattern: String, toPattern: String): String {
        var sdf = SimpleDateFormat(
            fromPattern,
            Locale.getDefault()
        )
        try {
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val d = sdf.parse(time)
            sdf = SimpleDateFormat(toPattern, Locale.GERMAN)
            sdf.timeZone = Calendar.getInstance().timeZone
            return sdf.format(d)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

    fun parseTimeUTCtoDefaultEnglish(time: String, fromPattern: String, toPattern: String): String {

        var sdf = SimpleDateFormat(
            fromPattern,
            Locale.getDefault()
        )
        try {
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val d = sdf.parse(time)
            sdf = SimpleDateFormat(toPattern, Locale.ENGLISH)
            sdf.timeZone = Calendar.getInstance().timeZone
            return sdf.format(d)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

    fun parseTimeUTCtoDefaultTurkey(time: String, fromPattern: String, toPattern: String): String {
        var sdf = SimpleDateFormat(
            fromPattern,
            Locale.getDefault()
        )
        try {
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val d = sdf.parse(time)
            sdf = SimpleDateFormat(toPattern, Locale("tr", "TR"))
            sdf.timeZone = Calendar.getInstance().timeZone
            return sdf.format(d)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    fun convertToTimeZone(
        date: String,
        frompattern: String,
        topattern: String,
        defaultzoneid: String,
        convertzoneid: String
    ): String {
        //Debug.e("TAG", "convertToTimeZone() called with: date = [" + date + "], frompattern = [" + frompattern + "], topattern = [" + topattern + "], defaultzoneid = [" + defaultzoneid + "], convertzoneid = [" + convertzoneid + "]");
        var returnDate = date
        try {
            TimeZone.setDefault(TimeZone.getTimeZone(convertzoneid))
            //Debug.e("Convert time zone", TimeZone.getDefault().getID());
            val inputDate = SimpleDateFormat(frompattern).parse(date)
            //Debug.e("Input Time", inputDate.toString());
            TimeZone.setDefault(TimeZone.getTimeZone(defaultzoneid))
            //Debug.e("Output time zone", TimeZone.getDefault().getID());
            val dateFormatGmt = SimpleDateFormat(topattern)
            dateFormatGmt.timeZone = TimeZone.getTimeZone(defaultzoneid)
            val dateFormatDefautl = SimpleDateFormat(topattern)
            val date1 = dateFormatDefautl.parse(dateFormatGmt.format(inputDate))
            val dateFormat = SimpleDateFormat(topattern)
            returnDate = dateFormat.format(date1)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            return returnDate
        }
    }

    fun getDaysDifference(startDate: Date, endDate: Date): Long {
        val sDate = getDatePart(startDate)
        val eDate = getDatePart(endDate)
        var daysBetween: Long = 0
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1)
            daysBetween++
        }
        return daysBetween
    }

    fun getFullDayName(day: Int): String {
        val c = Calendar.getInstance()
        // date doesn't matter - it has to be a Monday
        // I new that first August 2011 is one ;-)
        c.set(2011, 7, 1, 0, 0, 0)
        c.add(Calendar.DAY_OF_MONTH, day)
        return String.format("%tA", c)
    }

    fun getDatePart(date: Date): Calendar {
        val cal = Calendar.getInstance()       // get calendar instance
        cal.time = date
        cal.set(Calendar.HOUR_OF_DAY, 0)            // set hour to midnight
        cal.set(Calendar.MINUTE, 0)                 // set minute in hour
        cal.set(Calendar.SECOND, 0)                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0)            // set millisecond in second
        return cal                                  // return the date part
    }

    fun nullSafe(content: String?): String {
        if (content == null) {
            return ""
        }
        return if (content.equals("null", ignoreCase = true)) {
            ""
        } else content
    }

    fun nullSafe(content: String?, defaultStr: String): String {
        if (content == null) {
            return defaultStr
        }

        if (TextUtils.isEmpty(content)) {
            return defaultStr
        }
        return if (content.equals("null", ignoreCase = true)) {
            defaultStr
        } else content

    }

    fun nullSafeDash(content: String): String {
        if (TextUtils.isEmpty(content)) {
            return "-"
        }
        return if (content.equals("null", ignoreCase = true)) {
            "-"
        } else content
    }

    fun nullSafe(content: Int, defaultStr: String): String {
        return if (content == 0) {
            defaultStr
        } else "" + content
    }

    fun getTwoDecimalVal(value: Any?): String {
        return String.format("%.2f", value)
    }

    fun isSDcardMounted(): Boolean {
        val state = Environment.getExternalStorageState()
        return state == Environment.MEDIA_MOUNTED && state != Environment.MEDIA_MOUNTED_READ_ONLY
    }

    fun isGPSProviderEnabled(context: Context): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun isNetworkProviderEnabled(context: Context): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    fun isLocationProviderEnabled(context: Context): Boolean {
        return isGPSProviderEnabled(context) || isNetworkProviderEnabled(context)
    }

    fun isLocationProviderRequired(context: Context): Boolean {
        val lang = getPref(context, StaticDataUtility.USER_LONGITUDE, "")
        val lat = getPref(context, StaticDataUtility.USER_LATITUDE, "")
        if (lat != null) {
            if (lang != null) return !(lat.length > 0 && lang.length > 0)
        }
        return false
    }

    fun isUserLoggedIn(c: Context): Boolean {
        return getUserToken(c)!!.isNotEmpty()
    }

    fun getUserToken(c: Context): String? {
        return getPref(c, StaticDataUtility.sUSER_TOKEN, "")
    }

    fun getUid(c: Context): String? {
        return getPref(c, StaticDataUtility.sUSER_ID, "")!!
    }

//    fun getLoginDetails(c: Context): LogIn? {
//        val response = Utils.getPref(c, Constant.LOGIN_INFO, "")
//
//        if (response != null && response.length > 0) {
//
//            //            LogIn login = new Gson().fromJson(
//            //                    response, new TypeToken<LogIn>() {
//            //                    }.getType());
//            var login: LogIn? = null
//            try {
//                login = Gson().fromJson<LogIn>(JSONObject(response).toString(), object : TypeToken<LogIn>() {
//
//                }.type)
//            } catch (e: JSONException) {
//                e.printStackTrace()
//            }
//
//            if (login!!.responseCode === 200) {
//                return login
//            }
//
//        }
//        return null
//    }

//    fun getCartData(c: Context): CartData? {
//        val response = Utils.getPref(c, Constant.CART_DATA, "")
//
//        if (response != null && response.length > 0) {
//
//            //            LogIn login = new Gson().fromJson(
//            //                    response, new TypeToken<LogIn>() {
//            //                    }.getType());
//            var cartData: CartData? = null
//            try {
//                cartData = Gson().fromJson<CartData>(JSONObject(response).toString(), object : TypeToken<CartData>() {
//
//                }.type)
//            } catch (e: JSONException) {
//                e.printStackTrace()
//            }
//
//            if (cartData!!.responseCode === 200) {
//                return cartData
//            }
//
//        }
//        return null
//    }

//    fun getProfileDetails(c: Context): MyProfile? {
//        val response = Utils.getPref(c, Constant.PROFILE_INFO, "")
//
//        if (response != null && response.length > 0) {
//
//            //            LogIn login = new Gson().fromJson(
//            //                    response, new TypeToken<LogIn>() {
//            //                    }.getType());
//            var login: MyProfile? = null
//            try {
//                login = Gson().fromJson<MyProfile>(JSONObject(response).toString(), object : TypeToken<MyProfile>() {
//
//                }.type)
//            } catch (e: JSONException) {
//                e.printStackTrace()
//            }
//
//            if (login!!.responseCode === 200) {
//                return login
//            }
//
//        }
//        return null
//    }


    fun clearLoginCredetials(c: Activity) {
        delPref(c, StaticDataUtility.sUSER_TOKEN)
        delPref(c, StaticDataUtility.sUSER_ID)
//        Utils.delPref(c, RequestParamsUtils.USER_ID)
//        Utils.delPref(c, RequestParamsUtils.SESSION_ID)
        delPref(c, StaticDataUtility.LOGIN_INFO)
//        Utils.delPref(c, Constant.USER_LATITUDE)
//        Utils.delPref(c, Constant.USER_LONGITUDE)
//        Utils.delPref(c, RequestParamsUtils.TOKEN)
//        Utils.delPref(c, Constant.PROFILE_INFO)
//        Utils.delPref(c, Constant.COUNTRY_CODE)
//        Utils.delPref(c, Constant.CART_DATA)
//        Utils.delPref(c, Constant.ZIP_CODE)
//        Utils.delPref(c, Constant.IS_EMAIL_CONFIRM)
//        Utils.delPref(c, Constant.IS_ZIP_AVAILABLE)
//        Utils.delPref(c, Constant.ADDRESS_PRESELECT)
        val nMgr = c.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nMgr.cancelAll()
    }

    fun showDialog(c: Activity, title: String, message: String) {
        val builder = AlertDialog.Builder(c)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.btn_ok, DialogInterface.OnClickListener { dialog, which -> })

        val dialog = builder.create()
        dialog.show()
    }

    fun showDialog(c: Context, title: String, message: String, onClickListener: View.OnClickListener) {
        var builder: Dialog.Builder? = null
        builder = object : SimpleDialog.Builder(R.style.SimpleDialogLight) {
            override fun onPositiveActionClicked(fragment: DialogFragment) {
                super.onPositiveActionClicked(fragment)
                onClickListener.onClick(null)
            }
        }
        (builder as SimpleDialog.Builder).message("" + message)
            .title("" + title)
            .positiveAction("" + c.getString(R.string.btn_ok))
        builder.build(c).show()
    }

//    fun showDialog(c: Context, title: String, message: String,
//                   btnPos: String, btnNeg: String,
//                   onPosClickListener: View.OnClickListener,
//                   onNegClickListener: View.OnClickListener) {
//
//        var builder: Dialog.Builder? = null
//
//        builder = object : SimpleDialog.Builder(R.style.SimpleDialogLight) {
//
//            fun onPositiveActionClicked(fragment: DialogFragment) {
//                super.onPositiveActionClicked(fragment)
//                onPosClickListener.onClick(null)
//            }
//
//            fun onNegativeActionClicked(fragment: DialogFragment) {
//                super.onNegativeActionClicked(fragment)
//                onNegClickListener.onClick(null)
//            }
//
//        }
//
//        (builder as SimpleDialog.Builder).message("" + message)
//                .title("" + title)
//                .positiveAction("" + btnPos)
//                .negativeAction("" + btnNeg)
//
//        builder!!.build(c).show()
//    }

    fun asList(str: String): ArrayList<String?> {
        return ArrayList<String?>(
            Arrays.asList(
                *str
                    .split("\\s*,\\s*".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            )
        )
    }

    fun implode(data: ArrayList<String>): String {
        try {
            var devices = ""
            for (iterable_element in data) {
                devices = "$devices,$iterable_element"
            }
            if (devices.length > 0 && devices.startsWith(",")) {
                devices = devices.substring(1)
            }
            return devices
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    /**
     * Create a File for saving an image or video
     *
     * @return Returns file reference
     */
    fun getOutputMediaFile(): File? {
        try {
            // To be safe, you should check that the SDCard is mounted
            // using Environment.getExternalStorageState() before doing this.
            val mediaStorageDir: File
            if (isSDcardMounted()) {
                mediaStorageDir = File(StaticDataUtility.FOLDER_RIDEINN_PATH)
            } else {
                mediaStorageDir = File(
                    Environment.getRootDirectory(),
                    StaticDataUtility.FOLDER_NAME
                )
            }

            // This location works best if you want the created images to be
            // shared
            // between applications and persist after your app has been
            // uninstalled.
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null
                }
            }
            // Create a media file name
            val mediaFile = File(
                mediaStorageDir.path
                        + File.separator + Date().time + ".jpg"
            )
            mediaFile.createNewFile()
            return mediaFile
        } catch (e: Exception) {
            return null
        }
    }

    fun scanMediaForFile(context: Context, filePath: String) {
        resetExternalStorageMedia(context, filePath)
        notifyMediaScannerService(context, filePath)
    }

    fun resetExternalStorageMedia(context: Context, filePath: String): Boolean {
        try {
            if (Environment.isExternalStorageEmulated())
                return false
            val uri = Uri.parse("file://" + File(filePath))
            val intent = Intent(Intent.ACTION_MEDIA_MOUNTED, uri)
            context.sendBroadcast(intent)
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }

    fun notifyMediaScannerService(context: Context, filePath: String) {
        MediaScannerConnection.scanFile(
            context, arrayOf(filePath),
            null
        ) { path, uri ->
            Debug.i("ExternalStorage", "Scanned $path:")
            Debug.i("ExternalStorage", "-> uri=$uri")
        }
    }

    fun cancellAllNotication(context: Context) {
        val notificationManager = context
            .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
    }

    fun toInitCap(param: String?): String? {
        try {
            if (param != null && param.length > 0) {
                val charArray = param.toCharArray() // convert into char
                // array
                charArray[0] = Character.toUpperCase(charArray[0]) // set
                // capital
                // letter to
                // first
                // position
                return String(charArray) // return desired output
            } else {
                return ""
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return param
    }

    fun encodeTobase64(image: Bitmap): String {
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        val imageEncoded = Base64.encodeToString(b, Base64.DEFAULT)

        Debug.e("LOOK", imageEncoded)
        return imageEncoded
    }

    fun decodeBase64(input: String): Bitmap {
        val decodedByte = Base64.decode(input, 0)
        return BitmapFactory
            .decodeByteArray(decodedByte, 0, decodedByte.size)
    }

    fun getExtenstion(urlPath: String): String {
        if (urlPath.contains(".")) {
            val extension = urlPath.substring(urlPath.lastIndexOf(".") + 1)
            return urlPath.substring(urlPath.lastIndexOf(".") + 1)
        }
        return ""
    }

    fun getFileName(urlPath: String): String {
        return if (urlPath.contains(".")) {
            urlPath.substring(urlPath.lastIndexOf("/") + 1)
        } else ""
    }

    fun dpToPx(context: Context, `val`: Int): Float {
        val r = context.resources
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, `val`.toFloat(), r.displayMetrics)
    }

    fun spToPx(context: Context, `val`: Int): Float {
        val r = context.resources
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, `val`.toFloat(), r.displayMetrics)
    }

    fun noInternet(a: Activity) {
        showDialog(a, a.getString(R.string.connection_title), a.getString(R.string.connection_not_available))
    }

    fun getMimeType(url: String): String? {
        var type: String? = null
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
        Debug.e("type", "" + type!!)
        return type
    }

    fun isJPEGorPNG(url: String): Boolean {
        try {
            val type = getMimeType(url)
            val ext = type!!.substring(type.lastIndexOf("/") + 1)
            if (ext.equals("jpeg", ignoreCase = true) || ext.equals("jpg", ignoreCase = true) || ext.equals(
                    "png",
                    ignoreCase = true
                )
            ) {
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return true
        }
        return false
    }

    fun getFileSize(url: String): Double {
        val file = File(url)
        // Get length of file in bytes
        val fileSizeInBytes = file.length().toDouble()
        // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
        val fileSizeInKB = fileSizeInBytes / 1024
        // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
        val fileSizeInMB = fileSizeInKB / 1024

        Debug.e("fileSizeInMB", "" + fileSizeInMB)
        return fileSizeInMB
    }

    fun getAsteriskName(str: String): String? {
        var str = str
        val n = 4
        str = nullSafe(str)
        val fStr = StringBuilder()
        if (!TextUtils.isEmpty(str)) {
            if (str.length > n) {
                fStr.append(str.substring(0, n))
                for (i in 0 until str.length - n) {
                    fStr.append("*")
                }

                return fStr.toString()
            } else {
                fStr.append(str.substring(0, str.length - 1))
            }
        }
        return str
    }

    internal lateinit var dialog: android.app.Dialog

    fun twoDecimal(rate: String): String {
        if (!TextUtils.isEmpty(rate)) {
            val df = DecimalFormat("0.00", DecimalFormatSymbols(Locale.ENGLISH))
            return "CHF " + df.format(java.lang.Float.parseFloat(rate).toDouble())
        }
        return ""
    }

    fun twoDecimalWithoutChf(rate: String): String {
        if (!TextUtils.isEmpty(rate)) {
            val df = DecimalFormat("0.00", DecimalFormatSymbols(Locale.ENGLISH))
            return df.format(java.lang.Float.parseFloat(rate).toDouble())
        }
        return ""
    }

    fun toRequestBody(value: String): RequestBody {
        val body = RequestBody.create(MediaType.parse("multipart/form-data"), value)
        return RequestBody.create(MediaType.parse("multipart/form-data"), value)
    }


    fun getHashKey(c: Context) {
        // Add code to print out the key hash
        try {
            val info = c.packageManager.getPackageInfo(
                c.packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Debug.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }
    }

    fun getUserCountryCode(context: Context): String? {
        try {
            val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (tm != null) {
                val simCountry = tm.simCountryIso
                // String networkOperator = tm.getNetworkOperator();

                if (simCountry != null && simCountry.length == 2) { // SIM country code is available
                    /* if (isEuropeCountry(simCountry.toLowerCase(Locale.getDefault()), context)) {
                        Log.e("trueeeeee ", "trueeeee     &#8364");
                        return "&#8364";
                    } else {*/
                    return simCountry.toLowerCase(Locale.getDefault())
                    //   }
                } else if (tm.phoneType != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                    val networkCountry = tm.networkCountryIso
                    if (networkCountry != null && networkCountry.length == 2) { // network country code is available

                        /*if (isEuropeCountry(networkCountry.toLowerCase(Locale.getDefault()), context)) {
                            Log.e("trueeeeee ", "trueeeee     &#8364");
                            return "&#8364";
                        } else {*/
                        return networkCountry.toLowerCase(Locale.getDefault())
                        //  }
                        //  return networkCountry.toLowerCase(Locale.getDefault());
                    }
                }
            } else {
                return Locale.getDefault().country
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    object UtilCurrency {
        var currencyLocaleMap: SortedMap<Currency, Locale>

        init {
            currencyLocaleMap = TreeMap(Comparator { c1, c2 -> c1.currencyCode.compareTo(c2.currencyCode) })
            for (locale in Locale.getAvailableLocales()) {
                try {
                    val currency = Currency.getInstance(locale)
                    currencyLocaleMap[currency] = locale
                } catch (e: Exception) {
                }

            }
        }

        fun getCurrencySymbol(currencyCode: String): String {
            val currency = Currency.getInstance(currencyCode)
            var symbol = ""
            symbol = currency.getSymbol(currencyLocaleMap[currency])
            if (symbol.contains("$")) {
                symbol = "$"
            }
            //return currency.getSymbol(currencyLocaleMap.get(currency));
            return symbol
        }
    }

    fun getCurrencyCode(countryCode: String): String {
        return Currency.getInstance(Locale("", countryCode)).currencyCode
    }

    fun isToday(timeInMillis: Long): Boolean {
        val dateFormat = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
        val date = dateFormat.format(timeInMillis)
        return date == dateFormat.format(System.currentTimeMillis())
    }

    fun formatDate(timeInMillis: Long): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
        return dateFormat.format(timeInMillis)
    }

    /* public static boolean compareDate(Date date1, Date date2) {

//        if (date1.compareTo(date2) > 0) {
//            System.out.println("Date1 is after Date2");
//        } else if (date1.compareTo(date2) < 0) {
//            System.out.println("Date1 is before Date2");
//        } else if (date1.compareTo(date2) == 0) {
//            System.out.println("Date1 is equal to Date2");
//        } else {
//            System.out.println("How to get here?");
//        }
        return date1.compareTo(date2) == 0;
    }*/


    fun compareDate(date1: Date, date2: Date): Boolean {
        var date1 = date1
        var date2 = date2

        //        if (date1.compareTo(date2) > 0) {
        //            System.out.println("Date1 is after Date2");
        //        } else if (date1.compareTo(date2) < 0) {
        //            System.out.println("Date1 is before Date2");
        //        } else if (date1.compareTo(date2) == 0) {
        //            System.out.println("Date1 is equal to Date2");
        //        } else {
        //            System.out.println("How to get here?");
        //        }

        date1 = parseTime(date1, "yyyy-MM-dd")
        Debug.e("date 1", "" + date1.time)
        date2 = parseTime(date2, "yyyy-MM-dd")
        Debug.e("date 2", "" + date2.time)
        return date1.compareTo(date2) == 0
    }

    fun compareDateTommorrow(date1: Date): Boolean {
        //        if (date1.compareTo(date2) > 0) {
        //            System.out.println("Date1 is after Date2");
        //        } else if (date1.compareTo(date2) < 0) {
        //            System.out.println("Date1 is before Date2");
        //        } else if (date1.compareTo(date2) == 0) {
        //            System.out.println("Date1 is equal to Date2");
        //        } else {
        //            System.out.println("How to get here?");

        //        }
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrow = calendar.time
        Debug.e("tomorrow", tomorrow.toString())
        return compareDate(date1, tomorrow)
    }

    fun parseTime(time: Date, pattern: String): Date {
        val sdf = SimpleDateFormat(
            pattern,
            Locale.getDefault()
        )
        try {
            return sdf.parse(sdf.format(time))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time
    }

    fun dateIsGrater(firstDate: String, secondDate: String, pattern: String): Boolean {
//        val pattern = "yyyy-MM-dd HH:mm:ss"
        val dateFormat = SimpleDateFormat(pattern)
        try {
            val one = dateFormat.parse(firstDate)
            val two = dateFormat.parse(secondDate)
            return one.before(two)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return false
    }

    fun isActivityRunning(context: Context, activityName: String): Boolean {
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val activitys = activityManager.getRunningTasks(Integer.MAX_VALUE)
        var isActivityFound = false
        for (i in activitys.indices) {
            if (activitys[i].topActivity.toString().equals(
                    String.format(
                        "ComponentInfo{%s/%s}",
                        context.packageName,
                        activityName
                    ), ignoreCase = true
                )
            ) {
                isActivityFound = true
            }
        }
        return isActivityFound
    }

    fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo.packageName == context.packageName) {
                isInBackground = false
            }
        }

        return isInBackground
    }

    fun isMyServiceRunning(context: Context, serviceClass: Class<*>): Boolean {

        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {

            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    fun getConnectivityStatusString(context: Context): Boolean {
        val connection = getConnectivityStatus(context)
        var status = false
        status = connection == ConnectivityManager.TYPE_WIFI || connection ==
                ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE ||
                connection != TYPE_NOT_CONNECTED
        return status
    }


    fun getConnectivityStatus(context: Context): Int {
        val cm = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        if (null != activeNetwork && activeNetwork.isConnectedOrConnecting) {

            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI)
                return ConnectivityManager.TYPE_WIFI

            if (activeNetwork.type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                return ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE
        }
        return TYPE_NOT_CONNECTED
    }

    fun rotateBitmap(src: String, bitmap: Bitmap): Bitmap {
        try {
            val orientation = getExifOrientation(src)

            if (orientation == 1) {
                return bitmap
            }

            val matrix = Matrix()
            when (orientation) {
                2 -> matrix.setScale(-1f, 1f)
                3 -> matrix.setRotate(180f)
                4 -> {
                    matrix.setRotate(180f)
                    matrix.postScale(-1f, 1f)
                }
                5 -> {
                    matrix.setRotate(90f)
                    matrix.postScale(-1f, 1f)
                }
                6 -> matrix.setRotate(90f)
                7 -> {
                    matrix.setRotate(-90f)
                    matrix.postScale(-1f, 1f)
                }
                8 -> matrix.setRotate(-90f)
                else -> return bitmap
            }

            try {
                val oriented = Bitmap.createBitmap(
                    bitmap, 0, 0,
                    bitmap.width, bitmap.height, matrix, true
                )
                bitmap.recycle()
                return oriented
            } catch (e: OutOfMemoryError) {
                e.printStackTrace()
                return bitmap
            }

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return bitmap
    }

    @Throws(IOException::class)
    private fun getExifOrientation(src: String): Int {
        var orientation = 1

        try {
            /**
             * if your are targeting only api level >= 5 ExifInterface exif =
             * new ExifInterface(src); orientation =
             * exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
             */
            if (Build.VERSION.SDK_INT >= 5) {
                val exifClass = Class
                    .forName("android.media.ExifInterface")
                val exifConstructor = exifClass
                    .getConstructor(String::class.java)
                val exifInstance = exifConstructor
                    .newInstance(src)
                val getAttributeInt = exifClass.getMethod(
                    "getAttributeInt",
                    String::class.java, Int::class.javaPrimitiveType
                )
                val tagOrientationField = exifClass
                    .getField("TAG_ORIENTATION")
                val tagOrientation = tagOrientationField.get(null) as String
                orientation = getAttributeInt.invoke(
                    exifInstance,
                    tagOrientation, 1
                ) as Int
            }
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: SecurityException) {
            e.printStackTrace()
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        }

        return orientation
    }

    @Throws(IOException::class)
    fun getCorrectlyOrientedImage(context: Context, photoUri: Uri): Bitmap {
        var `is` = context.contentResolver.openInputStream(photoUri)
        val dbo = BitmapFactory.Options()
        dbo.inJustDecodeBounds = true
        BitmapFactory.decodeStream(`is`, null, dbo)
        `is`!!.close()

        val rotatedWidth: Int
        val rotatedHeight: Int
        val orientation = getOrientation(context, photoUri)

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight
            rotatedHeight = dbo.outWidth
        } else {
            rotatedWidth = dbo.outWidth
            rotatedHeight = dbo.outHeight
        }

        var srcBitmap: Bitmap?
        `is` = context.contentResolver.openInputStream(photoUri)
        if (rotatedWidth > 800 || rotatedHeight > 800) {
            val widthRatio = rotatedWidth.toFloat() / 800.toFloat()
            val heightRatio = rotatedHeight.toFloat() / 800.toFloat()
            val maxRatio = Math.max(widthRatio, heightRatio)

            // Create the bitmap from file
            val options = BitmapFactory.Options()
            options.inSampleSize = maxRatio.toInt()
            srcBitmap = BitmapFactory.decodeStream(`is`, null, options)
        } else {
            srcBitmap = BitmapFactory.decodeStream(`is`)
        }
        `is`!!.close()

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
        if (orientation > 0) {
            val matrix = Matrix()
            matrix.postRotate(orientation.toFloat())

            srcBitmap = Bitmap.createBitmap(
                srcBitmap!!, 0, 0,
                srcBitmap.width, srcBitmap.height, matrix, true
            )
        }

        return srcBitmap
    }

    fun getOrientation(context: Context, photoUri: Uri): Int {
        /* it's on the external media. */
        val cursor = context.contentResolver.query(
            photoUri,
            arrayOf(MediaStore.Images.ImageColumns.ORIENTATION), null, null, null
        )

        if (cursor!!.count != 1) {
            return -1
        }

        cursor.moveToFirst()
        return cursor.getInt(0)
    }

    fun refreshActivity(activity: Activity, intent: Intent) {
        activity.finish()
        activity.overridePendingTransition(0, 0)
        activity.startActivity(intent)
        activity.overridePendingTransition(0, 0)
    }

    fun pickGallery(context: Activity) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        context.startActivityForResult(
            Intent.createChooser(intent, "Select Picture"),
            Integer.parseInt(StaticDataUtility.PICK_IMAGE)
        )
    }

    fun base64(photoPath: String): String {
        val file = File(photoPath)
        val size = file.length().toInt()
        val bytes = ByteArray(size)
        try {
            val buf = BufferedInputStream(FileInputStream(file))
            buf.read(bytes, 0, bytes.size)
            buf.close()
        } catch (e: FileNotFoundException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        return Base64.encodeToString(bytes, Base64.DEFAULT)
    }

    fun nullSafeNA(content: String?): String {
        return if (content == null || content.isEmpty()) {
            "N/A"
        } else content
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                .matches()
        }
    }

    fun isGPSEnabled(context: Context): Boolean {

        return (context.getSystemService(Context.LOCATION_SERVICE) as LocationManager).isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun turnGPSOff(context: Context) {
        val provider = Settings.Secure.getString(context.contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
        if (provider.contains("gps")) { //if gps is enabled
            val poke = Intent()
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider")
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE)
            poke.data = Uri.parse("3")
            context.sendBroadcast(poke)
        }
    }

    fun showSingleDialog(c: Activity, title: String, message: String) {
        val dialog = AlertDialog.Builder(c)
            .setTitle(title)
            .setMessage(message)
            .setCancelable(true)
            .setPositiveButton(
                R.string.btn_ok
            ) { dialog, which ->
                if (dialogListener != null) {
                    dialogListener.onPositiveClick(dialog)
                }
                dialog.dismiss()
            }
            .setNegativeButton(
                R.string.btn_no
            ) { dialog, which ->
                if (dialogListener != null) {
                    dialogListener.onNegetiveClick(dialog)
                }
                dialog.dismiss()
            }.create()
        dialog.show()
        val nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE)
        nbutton.setTextColor(c.resources.getColor(R.color.colorPrimary))
        val ybutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE)
        ybutton.setTextColor(c.resources.getColor(R.color.colorPrimary))
    }

    fun getPath(): String {
        var path = ""
        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            path = Environment.getExternalStorageDirectory().absolutePath
        } else if (File("/mnt/emmc").exists()) {
            path = "/mnt/emmc"
        } else {
            path = Environment.getExternalStorageDirectory().absolutePath
        }
        return path
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    fun getPath(context: Context, uri: Uri): String? {

        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }

                // TODO handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {

                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(context, contentUri, selection, selectionArgs)
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    fun getDataColumn(
        context: Context, uri: Uri?, selection: String?,
        selectionArgs: Array<String>?
    ): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    fun setDialogListner(dialogListener: DialogClickListener) {
        this.dialogListener = dialogListener
    }

    internal interface DialogClickListener {
        fun onPositiveClick(dialog: DialogInterface)

        fun onNegetiveClick(dialog: DialogInterface)
    }

    private fun saveBitmap(fullPath: String?, bitmap: Bitmap?): Boolean {
        if (fullPath == null || bitmap == null)
            return false

        var fileCreated = false
        var bitmapCompressed = false
        var streamClosed = false

        val imageFile = File(fullPath)

        if (imageFile.exists())
            if (!imageFile.delete())
                return false

        try {
            fileCreated = imageFile.createNewFile()

        } catch (e: IOException) {
            e.printStackTrace()
        }

        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(imageFile)
            bitmapCompressed = bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)

        } catch (e: Exception) {
            e.printStackTrace()
            bitmapCompressed = false

        } finally {
            if (out != null) {
                try {
                    out.flush()
                    out.close()
                    streamClosed = true

                } catch (e: IOException) {
                    e.printStackTrace()
                    streamClosed = false
                }

            }
        }

        return fileCreated && bitmapCompressed && streamClosed
    }

    fun showDatePickerDialog(context: Context, textView: TextView) {
        val datePickerDialog = DatePickerDialog(
            context,
            DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                calendar.set(Calendar.MONTH, month)
                calendar.set(Calendar.YEAR, year)
                textView.text = parseTime(calendar.timeInMillis, StaticDataUtility.DATE_FORMAT)
            },
            Calendar.getInstance().get(Calendar.YEAR),
            Calendar.getInstance().get(Calendar.MONTH),
            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    fun showDatePickerDialog(context: Context, textView: TextView, dateFormat: String) {
        val datePickerDialog = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                calendar.set(Calendar.MONTH, month)
                calendar.set(Calendar.YEAR, year)
                textView.text = parseTime(calendar.timeInMillis, dateFormat)
            },
            Calendar.getInstance().get(Calendar.YEAR),
            Calendar.getInstance().get(Calendar.MONTH),
            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    fun showDatePickerDialog(
        context: Context,
        textView: TextView,
        dateFormat: String,
        selectedYear: Int,
        selectedMonth: Int,
        selectedDayOfMonth: Int
    ) {
        val datePickerDialog =
            DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                calendar.set(Calendar.MONTH, month)
                calendar.set(Calendar.YEAR, year)
                textView.text = parseTime(calendar.timeInMillis, dateFormat)
            }, selectedYear, (selectedMonth - 1), selectedDayOfMonth)
        datePickerDialog.show()
    }

    fun showDatePickerDialogWithMinDate(context: Context, textView: TextView) {
        val datePickerDialog = DatePickerDialog(
            context,
            DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                calendar.set(Calendar.MONTH, month)
                calendar.set(Calendar.YEAR, year)
                textView.text = parseTime(calendar.timeInMillis, StaticDataUtility.DATE_FORMAT)
            },
            Calendar.getInstance().get(Calendar.YEAR),
            Calendar.getInstance().get(Calendar.MONTH),
            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        )
        try {
            datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
        datePickerDialog.show()
    }

    fun showTimePickerDialog(context: Context, editText: EditText) {
        val timePickerDialog = TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.MILLISECOND, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
            calendar.set(Calendar.MINUTE, minute)
            editText.setText(parseTime(calendar.timeInMillis, StaticDataUtility.TIME_FORMAT))
        }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true)
        timePickerDialog.show()
    }

    fun menuIconWithText(drawable: Drawable, title: String): CharSequence {
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        val sb = SpannableString("    $title")
        val imageSpan = ImageSpan(drawable, ImageSpan.ALIGN_BOTTOM)
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return sb
    }

    fun hasPermissions(context: Context, permissions: Array<String>): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    /*fun sendEmail(context: Context, subject: String, message: String, emailAddress: String) {
//        val intent = Intent(Intent.ACTION_SEND)
//        intent.putExtra(Intent.EXTRA_EMAIL, emailAddress)
//        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
//        intent.putExtra(Intent.EXTRA_TEXT, message)
//        intent.type = "message/rfc822"
//        context.startActivity(Intent.createChooser(intent, "Send Email using:"))

        val intent = Intent(Intent.ACTION_SEND).setType("text/plain").putExtra(Intent.EXTRA_EMAIL, arrayOf<String>(emailAddress)
        intent.type = "text/html"
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject)
        intent.putExtra(android.content.Intent.EXTRA_TEXT, message)
        val matches = context.packageManager.queryIntentActivities(intent, 0)
        var best: ResolveInfo? = null
        for (info in matches) {
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail")) {
                best = info
            }
        }
        if (best != null) {
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name)
        }
        context.startActivity(intent)
    }*/

    fun openPlayStore(context: Context) {
        try {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${context.packageName}")))
        } catch (anfe: android.content.ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=${context.packageName}")
                )
            )
        }
    }

    fun openWeb(context: Context, url: String) {
        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    fun shareWhatsapp(context: Context, content: String?) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.`package` = "com.whatsapp"
        intent.putExtra(Intent.EXTRA_TEXT, nullSafe(content))
        try {
            context.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun sendFeedBack(activity: Activity, emailAddress: String) {
        sendEMailWithInfo(
            activity,
            "Send Feedback",
            "${activity.resources.getString(R.string.app_name)} Feedback",
            emailAddress
        )
    }

    fun sendEMailWithInfo(activity: Activity, title: String, subject: String, emailAddress: String) {
        val mailDescription = "\n\n\n\n\n\n\n-----------------------------------------------------------\n" +
                "Don't delete the following info. It will help us to locate the problem.\n" +
                "-----------------------------------------------------------\n" +
                "App version: " + " v" + BuildConfig.VERSION_NAME + "\n" +
                "Device: " + getPhoneBrand(activity) + " " + getPhoneModel(activity) + "\n" +
                "Device OS Version: " + getOsVersion(activity) + "\n" +
                "API Level: " + getOsAPILevel(activity) + "\n"
        val result = Intent(Intent.ACTION_SENDTO)
        result.data = Uri.parse("mailto:")
        result.putExtra(Intent.EXTRA_EMAIL, arrayOf(emailAddress))
        result.putExtra(Intent.EXTRA_SUBJECT, subject)
        result.putExtra(Intent.EXTRA_TEXT, mailDescription)
        val matches = activity.packageManager.queryIntentActivities(result, 0)
        var best: ResolveInfo? = null
        for (info in matches) {
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail")) {
                best = info
            }
        }
        if (best != null) {
            result.setClassName(best.activityInfo.packageName, best.activityInfo.name)
        }
        activity.startActivity(Intent.createChooser(result, title))
    }

    fun shareApp(activity: Activity) {
        val shareText =
            "Hey guys, Try out this amazing ${activity.resources.getString(R.string.app_name)} app!. " + Global.getAppShareUri(
                activity
            ).toString()
        ShareCompat.IntentBuilder
            .from(activity)
            .setText(shareText)
            .setType("text/plain")
            .setChooserTitle("Share ${activity.resources.getString(R.string.app_name)}")
            .startChooser()
    }

    fun shareProduct(activity: Activity, shareText: String) {
        ShareCompat.IntentBuilder
            .from(activity)
            .setText(shareText)
            .setType("text/plain")
            .setChooserTitle("Share Via ${activity.resources.getString(R.string.app_name)}")
            .startChooser()
    }

    fun getAppShareUri(context: Context): Uri {
        return Uri.parse("https://play.google.com/store/apps/details?id=" + context.packageName)
    }

    fun visitFacebookPage(context: Context, fbUrl: String) {
//        val fbUrl = "https://www.facebook.com/Reconeco-Outdoors-2042158022499980/"
        try {
            context.startActivity(facebookIntent(context.packageManager, fbUrl))
        } catch (e: Exception) {
            openWeb(context, fbUrl)
        }
    }

    fun visitInstagramPage(context: Context, instaUrl: String) {
//        val instaUrl = "http://instagram.com/_u/reconeco_outdoors"
        val uri = Uri.parse(instaUrl)
        val likeIng = Intent(Intent.ACTION_VIEW, uri)
        likeIng.setPackage("com.instagram.android")
        try {
            context.startActivity(likeIng)
        } catch (e: ActivityNotFoundException) {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(instaUrl)))
        }
    }

    //region FOR GET FACEBOOK INTENT
    private fun facebookIntent(pm: PackageManager, url: String): Intent {
        var uri = Uri.parse(url)
        try {
            val applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0)
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=$url")
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        }

        return Intent(Intent.ACTION_VIEW, uri)
    }
    //endregion

    fun visitYoutubePage(context: Context, youtubeUrl: String) {
        openWeb(context, youtubeUrl)
    }

    fun visitTwitterPage(context: Context, twitterId: String) {
        var intent: Intent
        try {
            //GET THE TWITTER APP IF POSSIBLE
            context.packageManager.getPackageInfo("com.twitter.android", 0)
            intent = Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=$twitterId"))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        } catch (e: java.lang.Exception) {
            //NO TWITTER APP, REVERT TO BROWSER
            intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/$twitterId"))
        }
        context.startActivity(intent)
    }

    fun openShareDialog(context: Context, content: String?) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, nullSafe(content))
        context.startActivity(intent)
    }

    fun multiSelectionDialog(context: Context) {
        val list = ArrayList<String>()
        list.add("Masterpiece")
        val items = list.toTypedArray()
        val selectedItems = ArrayList<Int>()

        val builder = AlertDialog.Builder(context)
            .setTitle("Choose Item")
            .setMultiChoiceItems(items, null) { dialog, which, isChecked ->
                if (isChecked) {
                    selectedItems.add(which)
                } else if (selectedItems.contains(which)) {
                    selectedItems.remove(which)
                }
            }
            .setPositiveButton(R.string.btn_enable) { dialog, which -> }
            .setNegativeButton(R.string.btn_cancel) { dialog, which -> }
            .setCancelable(true)
        val dialog = builder.show()
    }

    fun singleSelectionDialog(context: Context) {
        val list = ArrayList<String>()
        list.add("Masterpiece")
        val items = list.toTypedArray()

        val builder = AlertDialog.Builder(context)
            .setTitle("Choose Item")
            .setSingleChoiceItems(items, 1) { dialog, which ->
                dialog.dismiss()
            }
            .setNegativeButton(R.string.btn_cancel) { dialog, which -> }
            .setCancelable(true)
        val dialog = builder.show()
    }

    @SuppressLint("PackageManagerGetSignatures")
    fun getKeyHash(context: Context) {
        try {
            val info = context.packageManager.getPackageInfo(context.packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Debug.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: Exception) {
            sendExceptionReport(e)
        }
    }

    fun toMD5(string: String): String {
        try {
            val md5 = MessageDigest.getInstance("MD5")
            md5.update(string.toByteArray(Charsets.UTF_8))
            val md5bytes = md5.digest()
            val haxm5 = StringBuilder()
            for (b in md5bytes) {
                if (b and 0xFF.toByte() < 0x10)
                    haxm5.append("0")
                haxm5.append(Integer.toHexString((b and 0xFF.toByte()).toInt()))
            }
            return haxm5.toString()
        } catch (e: Exception) {
            sendExceptionReport(e)
        }
        return ""
    }

    fun encodeToBase64(bitmap: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        val imageEncoded = Base64.encodeToString(b, Base64.DEFAULT)

        Debug.e("Encode", imageEncoded)
        return imageEncoded
    }

    fun isEmpty(editText: EditText): Boolean {
        return editText.text.toString().isEmpty()
    }

    fun getDifference(date1: Date, date2: Date): String {
        val millis = date2.time - date1.time
        val hours = (millis / (1000 * 60 * 60)).toInt()
        val mins = (millis % (1000 * 60 * 60)).toInt()
        val diffSeconds = millis / 1000 % 60
        val diffMinutes = millis / (60 * 1000) % 60
        val diffHours = millis / (60 * 60 * 1000)
        val diffInDays = millis.toInt() / (1000 * 60 * 60 * 24)
        return "$diffHours hour $diffMinutes min $diffSeconds sec"
    }

    fun calculateTimeDifference(strDateFormat: String, strStartDate: String, strEndDate: String) {
        val simpleDateFormat = SimpleDateFormat(strDateFormat)
        val startDate: Date = simpleDateFormat.parse(strStartDate)
        val endDate: Date = simpleDateFormat.parse(strEndDate)

        var difference: Long = endDate.time - startDate.time
        if (difference < 0) {
            val dateMax: Date = simpleDateFormat.parse("24:00")
            val dateMin: Date = simpleDateFormat.parse("00:00")
            difference = (dateMax.time - startDate.time) + (endDate.time - dateMin.time)
        }
        val days: Int = ((difference / (1000 * 60 * 60 * 24)).toInt())
        val hours: Int = (((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60)).toInt())
        val min: Int = ((difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60)).toInt()
    }

    fun getIncrementDayDate(date: String, dateFormat: String, days: Int): String? {
        val inputFormat = SimpleDateFormat(dateFormat)
        val myDate = inputFormat.parse(date)
        val calendar = Calendar.getInstance()
        calendar.time = myDate
        calendar.add(Calendar.DAY_OF_YEAR, days)
        val newDate: Date = calendar.time
        return SimpleDateFormat("dd/MM/yyyy").format(newDate)
    }

    fun getCurrentDate(requiredFormat: String): String {
        val sdf = SimpleDateFormat(requiredFormat)
        return sdf.format(Date())
    }

    fun getCurrentDayNumberOfWeek(): Int {
        val c = Calendar.getInstance()
        return c.get(Calendar.DAY_OF_WEEK)
    }

    fun getCurrentDayOfWeek(): String {
        val sdf = SimpleDateFormat("EEEE")
        val d = Date()
        return sdf.format(d)
    }

    fun getPhoneModel(): String {
        return try {
            Build.MODEL
        } catch (e: Exception) {
            sendExceptionReport(e)
            ""
        }
    }

    fun getPhoneBrand(): String {
        return try {
            Build.BRAND
        } catch (e: Exception) {
            sendExceptionReport(e)
            ""
        }
    }

    fun getOsVersion(): String {
        return try {
            Build.VERSION.RELEASE
        } catch (e: Exception) {
            sendExceptionReport(e)
            ""
        }
    }

    fun getOsAPILevel(): Int {
        return try {
            Build.VERSION.SDK_INT
        } catch (e: Exception) {
            sendExceptionReport(e)
            0
        }
    }

    @SuppressLint("SimpleDateFormat")
    //region FOR CHANGE DATE-FORMAT
    fun changeDateFormat(value: String, yourFormat: String, requiredFormat: String): String {
        val inputFormat = SimpleDateFormat(yourFormat)
        val outputFormat = SimpleDateFormat(requiredFormat)

        var resultString = ""
        try {
            val date = inputFormat.parse(value)
            resultString = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return resultString
    }
    //endregion

    fun validatePassword(password: String): Boolean {
        val regex = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,15}$"
        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }

    //region FOR GET REAL PATH FROM URI
    fun getRealPathFromURI(uri: Uri, context: Context): String {
        var path = ""
        if (context.contentResolver != null) {
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }
    //endregion


    fun getFileFormat(id: String): String {
        val format = System.currentTimeMillis().toString() + "_0"
        if (id.endsWith("_1")) {
            return ".jpg"
        }
        if (id.endsWith("_2")) {
            return ".JPG"
        }
        if (id.endsWith("_3")) {
            return ".png"
        }
        if (id.endsWith("_4")) {
            return ".PNG"
        }
        if (id.endsWith("_5")) {
            return ".jpeg"
        }
        if (id.endsWith("_6")) {
            return ".JPEG"
        }
        if (id.endsWith("_7")) {
            return ".mp4"
        }
        if (id.endsWith("_8")) {
            return ".3gp"
        }
        if (id.endsWith("_9")) {
            return ".flv"
        }
        if (id.endsWith("_10")) {
            return ".m4v"
        }
        if (id.endsWith("_11")) {
            return ".avi"
        }
        if (id.endsWith("_12")) {
            return ".wmv"
        }
        if (id.endsWith("_13")) {
            return ".mpeg"
        }
        if (id.endsWith("_14")) {
            return ".VOB"
        }
        if (id.endsWith("_15")) {
            return ".MOV"
        }
        if (id.endsWith("_16")) {
            return ".MPEG4"
        }
        if (id.endsWith("_17")) {
            return ".DivX"
        }
        return if (id.endsWith("_18")) {
            ".mkv"
        } else format
    }

    fun genrateFileId(filePath: String): String {
        try {
            Thread.sleep(10)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        val format = "" + System.currentTimeMillis()
        if (filePath.endsWith(".jpg")) {
            return StringBuilder(format).append("_1").toString()
        }
        if (filePath.endsWith(".JPG")) {
            return StringBuilder(format).append("_2").toString()
        }
        if (filePath.endsWith(".png")) {
            return StringBuilder(format).append("_3").toString()
        }
        if (filePath.endsWith(".PNG")) {
            return StringBuilder(format).append("_4").toString()
        }
        if (filePath.endsWith(".jpeg")) {
            return StringBuilder(format).append("_5").toString()
        }
        if (filePath.endsWith(".JPEG")) {
            return StringBuilder(format).append("_6").toString()
        }
        if (filePath.endsWith(".mp4")) {
            return StringBuilder(format).append("_7").toString()
        }
        if (filePath.endsWith(".3gp")) {
            return StringBuilder(format).append("_8").toString()
        }
        if (filePath.endsWith(".flv")) {
            return StringBuilder(format).append("_9").toString()
        }
        if (filePath.endsWith(".m4v")) {
            return StringBuilder(format).append("_10").toString()
        }
        if (filePath.endsWith(".avi")) {
            return StringBuilder(format).append("_11").toString()
        }
        if (filePath.endsWith(".wmv")) {
            return StringBuilder(format).append("_12").toString()
        }
        if (filePath.endsWith(".mpeg")) {
            return StringBuilder(format).append("_13").toString()
        }
        if (filePath.endsWith(".VOB")) {
            return StringBuilder(format).append("_14").toString()
        }
        if (filePath.endsWith(".MOV")) {
            return StringBuilder(format).append("_15").toString()
        }
        if (filePath.endsWith(".MPEG4")) {
            return StringBuilder(format).append("_16").toString()
        }
        if (filePath.endsWith(".DivX")) {
            return StringBuilder(format).append("_17").toString()
        }
        return if (filePath.endsWith(".mkv")) {
            StringBuilder(format).append("_18").toString()
        } else format
    }

    @SuppressLint("DefaultLocale")
    fun getDuration(duration: Long): String {
        if (duration < 1000) {
            return String.format("%02d:%02d", *arrayOf<Any>(Integer.valueOf(0), Integer.valueOf(0)))
        }
        val n = duration / 1000
        val n2 = n / 3600
        val n3 = n % 3600 / 60
        val n4 = n - (3600 * n2 + 60 * ((n - 3600 * n2) / 60))
        return if (n2 == 0L) {
            String.format("%02d:%02d", *arrayOf<Any>(java.lang.Long.valueOf(n3), java.lang.Long.valueOf(n4)))
        } else String.format(
            "%02d:%02d:%02d",
            *arrayOf<Any>(java.lang.Long.valueOf(n2), java.lang.Long.valueOf(n3), java.lang.Long.valueOf(n4))
        )
    }

    private fun putPrefixZero(n: Long): String {
        return if (n.toString().length < 2) {
            "0$n"
        } else "" + n
    }

    fun readPatternData(activity: Context): CharArray? {
        try {
            if (File(activity.filesDir.toString() + "/pattern").exists()) {
                val objectInputStream = ObjectInputStream(FileInputStream(activity.filesDir.toString() + "/pattern"))
                val array = objectInputStream.readObject() as CharArray
                objectInputStream.close()
                return array
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

    @Throws(IOException::class)
    fun moveFile(src: File, des: File) {
        if (src.exists() && !src.renameTo(des)) {
            if (!des.parentFile.exists()) {
                des.parentFile.mkdirs()
            }
            val `in` = FileInputStream(src)
            val out = FileOutputStream(des)
            val buf = ByteArray(1024)
            while (true) {
                val len = `in`.read(buf)
                if (len <= 0) {
                    break
                }
                out.write(buf, 0, len)
            }
            `in`.close()
            out.close()
            if (src.exists()) {
                src.delete()
            }
        }
    }

    //region FOR MOVE FILE
    @Throws(IOException::class)
    fun moveFile(sourceLocation: String, targetLocation: String) {
        if (!File(sourceLocation).renameTo(File(targetLocation))) {
            val file = File(sourceLocation)
            if (!file.parentFile.exists()) {
                file.parentFile.mkdirs()
            }
            val `in` = FileInputStream(sourceLocation)
            val out = FileOutputStream(targetLocation)
            val buf = ByteArray(1024)
            while (true) {
                val len = `in`.read(buf)
                if (len <= 0) {
                    break
                }
                out.write(buf, 0, len)
            }
            `in`.close()
            out.close()
            val deleteFile = File(sourceLocation)
            if (deleteFile.exists()) {
                deleteFile.delete()
            }
        }
    }
    //endregion

    //region FOR DELETE FILE
    fun deleteFile(mFile: File?): Boolean {
        var idDelete = false
        if (mFile == null) {
            return false
        }
        if (mFile.exists()) {
            if (mFile.isDirectory) {
                val children = mFile.listFiles()
                if (children != null && children.size > 0) {
                    for (child in children) {
                        mDeleteFileCount += child.length()
                        idDelete = deleteFile(child)
                    }
                }
                mDeleteFileCount += mFile.length()
                idDelete = mFile.delete()
            } else {
                mDeleteFileCount += mFile.length()
                idDelete = mFile.delete()
            }
        }
        return idDelete
    }
    //endregion

    //region FOR DELETE FILE
    fun deleteFile(s: String): Boolean {
        return deleteFile(File(s))
    }
    //endregion

    //region FOR START COUNT DOWN TIMER FOR HOUR-MINUTE-SECOND
    fun startCountDownTimerForHourMinuteSecond(
        tvCountDownTimer: TextView,
        millisInFuture: Long,
        countDownInterval: Long
    ) {
        val countDownTimer = object : CountDownTimer(millisInFuture, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                //Convert milliseconds into hour,minute and seconds
                val hms = String.format(
                    "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished)
                    ),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                    )
                )
                tvCountDownTimer.text = hms//set text
            }

            override fun onFinish() {
                tvCountDownTimer.text = "TIME'S UP!!" //On finish change timer text
            }
        }.start()
    }
    //endregion

    //region FOR START COUNT DOWN TIMER FOR MINUTE SECOND
    fun startCountDownTimerForMinuteSecond(tvCountDownTimer: TextView, millisInFuture: Long, countDownInterval: Long) {
        object : CountDownTimer(millisInFuture, countDownInterval) { // adjust the milli seconds here
            override fun onTick(millisUntilFinished: Long) {
                tvCountDownTimer.text = "" + String.format(
                    "%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(
                            millisUntilFinished
                        )
                    )
                )
            }

            override fun onFinish() {
                tvCountDownTimer.text = "done!"
            }
        }.start()
    }
    //endregion

    //region FOR START COUNT DOWN TIMER FOR SECOND
    fun startCountDownTimerForSecond(context: Context, button: Button, millisInFuture: Long, countDownInterval: Long) {
        button.isEnabled = false
        object : CountDownTimer(millisInFuture, countDownInterval) { // adjust the milli seconds here
            override fun onTick(millisUntilFinished: Long) {
                button.text = "" + String.format(
                    "%02d",
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(
                            millisUntilFinished
                        )
                    )
                ) + " sec"
            }

            override fun onFinish() {
                button.isEnabled = true
//                button.text = context.resources.getString(R.string._50_coins)
            }
        }.start()
    }
    //endregion

    //region FOR GET PATH WITHOUT FILE NAME
    fun getPathWithoutFilename(file: File?): File? {
        if (file != null) {
            return if (file.isDirectory) {
                // NO FILE TO BE SPLIT OFF. RETURN EVERYTHING
                file
            } else {
                val filename = file.name
                val filepath = file.absolutePath

                // CONSTRUCT PATH WITHOUT FILE NAME.
                var pathwithoutname = filepath.substring(0, filepath.length - filename.length)
                if (pathwithoutname.endsWith("/")) {
                    pathwithoutname = pathwithoutname.substring(0, pathwithoutname.length - 1)
                }
                File(pathwithoutname)
            }
        }
        return null
    }
    //endregion

    //region FOR GET FOLDER SIZE
    fun folderSize(directory: File): Long {
        var length: Long = 0
        val files = directory.listFiles()
        if (files != null)
            for (file in files)
                length += if (file.isFile)
                    file.length()
                else
                    folderSize(file)
        return length
    }
    //endregion

    //region FOR GET NAME WITHOUT EXTENSION
    fun getNameWithoutExtension(f: File): String {
        return f.name.substring(0, f.name.length - getExtension(getUri(f).toString())!!.length)
    }
    //endregion

    //region FOR GET URI FROM FILE
    fun getUri(file: File?): Uri? {
        return if (file != null) {
            Uri.fromFile(file)
        } else null
    }
    //endregion

    //region FOR CHECK IF ZIP ARCHIVE
    fun checkIfZipArchive(f: File): Boolean {
        val l = f.name.length
        // TODO test
        return f.isFile && getExtension(f.absolutePath).equals(".zip", ignoreCase = true)

        // Old way. REALLY slow. Too slow for realtime action loading.
        //        try {
        //            new ZipFile(f);
        //            return true;
        //        } catch (Exception e){
        //            return false;
        //        }
    }
    //endregion

    //region GET EXTENSION OF A FILE NAME, LIKE .PNG OR .JPG
    fun getExtension(uri: String?): String? {
        if (uri == null) {
            return null
        }

        val dot = uri.lastIndexOf(".")
        return if (dot >= 0) {
            uri.substring(dot)
        } else {
            // NO EXTENSION.
            ""
        }
    }
    //endregion

    //region FOR RESIZE BITMAP
    fun resizeBitmap(drawable: Bitmap, desireWidth: Int, desireHeight: Int): Bitmap {
        var drawable = drawable
        val width = drawable.width
        val height = drawable.height

        if (0 < width && 0 < height && desireWidth < width || desireHeight < height) {
            // CALCULATE SCALE
            var scale: Float
            if (width < height) {
                scale = desireHeight.toFloat() / height.toFloat()
                if (desireWidth < width * scale) {
                    scale = desireWidth.toFloat() / width.toFloat()
                }
            } else {
                scale = desireWidth.toFloat() / width.toFloat()
            }

            // Draw resized image
            val matrix = Matrix()
            matrix.postScale(scale, scale)
            val bitmap = Bitmap.createBitmap(
                drawable, 0, 0, width, height,
                matrix, true
            )
            val canvas = Canvas(bitmap)
            canvas.drawBitmap(bitmap, 0f, 0f, null)
            drawable = bitmap
        }
        return drawable
    }
    //endregion

    //region FOR RESIZE DRAWABLE
    fun resizeDrawable(drawable: Drawable, desireWidth: Int, desireHeight: Int): Drawable {
        var dr = drawable
        val width = drawable.intrinsicWidth
        val height = drawable.intrinsicHeight

        if (0 < width && 0 < height && desireWidth < width || desireHeight < height) {
            if (drawable is BitmapDrawable) {
                val b = drawable.bitmap
                val resized = Bitmap.createScaledBitmap(
                    b, desireWidth,
                    desireHeight, true
                )
                dr = BitmapDrawable(resized)
            }
        }
        return dr
    }
    //endregion

    //region FOR VECTOR TO BITMAP
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun vectorToBitmap(vectorDrawable: VectorDrawable): Bitmap {
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
        vectorDrawable.draw(canvas)
        Log.e("", "getBitmap: 1")
        return bitmap
    }
    //endregion

    //region FOR SVG TO BITMAP
    private fun svgToBitmap(context: Context, drawableId: Int): Bitmap {
        Log.e("", "getBitmap: 2")
        val drawable = ContextCompat.getDrawable(context, drawableId)
        return when (drawable) {
            is BitmapDrawable -> BitmapFactory.decodeResource(context.resources, drawableId)
            is VectorDrawable -> vectorToBitmap((drawable as VectorDrawable?)!!)
            else -> throw IllegalArgumentException("unsupported drawable type")
        }
    }
    //endregion

    //region FOR GET DAY SUFFIX
    fun getDaySuffix(n: Int): String {
        if (n in 11..13) {
            return "th"
        }
        return when (n % 10) {
            1 -> "st"
            2 -> "nd"
            3 -> "rd"
            else -> "th"
        }
    }
    //endregion

    //region FOR CHANGE DATE-FORMAT WITH SUFFIX
    fun changeDateFormatWithSuffix(value: String, yourFormat: String, requiredFormat: String): String {
        var result = ""
        try {
            val formatterOld = SimpleDateFormat(yourFormat, Locale.getDefault())
            formatterOld.timeZone = TimeZone.getTimeZone("UTC")
            var date: Date? = null
            date = formatterOld.parse(value)
            val dayFormat = SimpleDateFormat("d", Locale.getDefault())
            val day = dayFormat.format(date)
            val formatterNew =
                SimpleDateFormat("d'" + getDaySuffix(day.toInt()) + "' MMMM',' yyyy", Locale.getDefault())
            if (date != null) {
                result = formatterNew.format(date)
            }
        } catch (e: ParseException) {
            e.printStackTrace()
            return value
        }
        return result
    }
    //endregion

    //region FOR ENCODED IMAGE
    fun encodePNGImage(path: String): String {
        val imageFile = File(path)
        var fis: FileInputStream? = null
        try {
            fis = FileInputStream(imageFile)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        val bm = BitmapFactory.decodeStream(fis)
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.PNG, 70, baos)
        val b = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }
    //endregion

    //region FOR DECODE IMAGE
    private fun decodeImage(data: String): Bitmap {
        val b = Base64.decode(data, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(b, 0, b.size)
    }
    //endregion

    //region FOR CHANGE BITMAP COLOR
    fun changeBitmapColor(bitmap: Bitmap, color: Int): Bitmap {
        val bmpRedScale = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)

        val canvas = Canvas(bmpRedScale)
        val paint = Paint()

        paint.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, 0f, 0f, paint)

        return bmpRedScale
    }
    //endregion

    //region CHECK FOR IS TABLET DEVICE
    fun isTablet(context: Activity): Boolean {
        val metrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(metrics)
        val widthPixels = metrics.widthPixels
        val heightPixels = metrics.heightPixels
        val scaleFactor = metrics.density
        val widthDp = widthPixels / scaleFactor
        val heightDp = heightPixels / scaleFactor
        val smallestWidth = Math.min(widthDp, heightDp)
        return when {
            smallestWidth > 720 -> //DEVICE IS A 10" TABLET
                true
            smallestWidth > 600 -> //DEVICE IS A 7" TABLET
                true
            else -> false
        }
    }
    //endregion

    //region CHECK FOR IS TABLET DEVICE
    fun isTabletDevice(activityContext: Context): Boolean {
        val deviceLarge =
            activityContext.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_LARGE
        val metrics = DisplayMetrics()
        val activity = activityContext as Activity
        activity.windowManager.defaultDisplay.getMetrics(metrics)
        if (deviceLarge) {
            //TABLET
            when {
                metrics.densityDpi == DisplayMetrics.DENSITY_DEFAULT -> return true
                metrics.densityDpi == DisplayMetrics.DENSITY_MEDIUM -> return true
                metrics.densityDpi == DisplayMetrics.DENSITY_TV -> return true
                metrics.densityDpi == DisplayMetrics.DENSITY_HIGH -> return true
                metrics.densityDpi == DisplayMetrics.DENSITY_280 -> return true
                metrics.densityDpi == DisplayMetrics.DENSITY_XHIGH -> return true
                metrics.densityDpi == DisplayMetrics.DENSITY_400 -> return true
                metrics.densityDpi == DisplayMetrics.DENSITY_XXHIGH -> return true
                metrics.densityDpi == DisplayMetrics.DENSITY_560 -> return true
                metrics.densityDpi == DisplayMetrics.DENSITY_XXXHIGH -> return true
            }
        } else {
            //MOBILE
        }
        return false
    }
    //endregion

    //region FOR GET STATUS BAR HEIGHT
    fun getStatusBarHeight(context: Context): Int {
        var result = 0
        val resourceId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = context.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }
    //endregion

    //region FOR GET ONLY CHARACTER FROM STRING
    fun getOnlyCharacter(s: String): String {
        val pattern = Pattern.compile("[^a-z A-Z]")
        val matcher = pattern.matcher(s)
        return matcher.replaceAll("")
    }
    //endregion

    //region FOR GET ONLY DIGITS FROM STRING
    fun getOnlyDigits(s: String): String {
        val pattern = Pattern.compile("[^0-9]")
        val matcher = pattern.matcher(s)
        return matcher.replaceAll("")
    }
    //endregion

    fun getDeviceType(c: Activity): String {
        val uiModeManager = c.getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
        return when (uiModeManager.currentModeType) {
            Configuration.UI_MODE_TYPE_TELEVISION -> "TELEVISION"
            Configuration.UI_MODE_TYPE_WATCH -> "WATCH"
            Configuration.UI_MODE_TYPE_NORMAL -> {
                if (isTablet(c)) "TABLET" else "PHONE"
            }
            Configuration.UI_MODE_TYPE_UNDEFINED -> "UNKOWN"
            else -> ""
        }
    }

    @JvmStatic
    fun smartUrlFilter(searchValue: String, canBeSearch: Boolean, searchUrl: String): String {
        val ACCEPTED_URI_SCHEMA =
            Pattern.compile("(?i)((?:http|https|file)://|(?:inline|data|about|javascript):|(?:.*:.*@))(.*)")
        val URL_ENCODED_SPACE = "%20"
        val QUERY_PLACE_HOLDER = "%s"

        var inUrl = searchValue.trim()
        val hasSpace = inUrl.contains(' ')
        val matcher = ACCEPTED_URI_SCHEMA.matcher(inUrl)
        if (matcher.matches()) {
            // force scheme to lowercase
            val scheme = matcher.group(1)
            val lcScheme = scheme.toLowerCase()
            if (lcScheme != scheme) {
                inUrl = lcScheme + matcher.group(2)
            }
            if (hasSpace && Patterns.WEB_URL.matcher(inUrl).matches()) {
                inUrl = inUrl.replace(" ", URL_ENCODED_SPACE)
            }
            return inUrl
        }
        if (!hasSpace) {
            if (Patterns.WEB_URL.matcher(inUrl).matches()) {
                return URLUtil.guessUrl(inUrl)
            }
        }

        return if (canBeSearch) {
            URLUtil.composeSearchUrl(
                inUrl,
                searchUrl, QUERY_PLACE_HOLDER
            )
        } else {
            ""
        }
    }

    fun getYoutubeThumbnailUrlFromVideoUrl(videoUrl: String): String {
        return "http://img.youtube.com/vi/" + getYoutubeVideoIdFromUrl(videoUrl) + "/0.jpg"
    }

    fun getYoutubeVideoIdFromUrl(inUrl: String): String? {
        var inUrl = inUrl
        inUrl = inUrl.replace("&feature=youtu.be", "")
        if (inUrl.toLowerCase().contains("youtu.be")) {
            return inUrl.substring(inUrl.lastIndexOf("/") + 1)
        }
        val pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*"
        val compiledPattern = Pattern.compile(pattern)
        val matcher = compiledPattern.matcher(inUrl)
        return if (matcher.find()) {
            matcher.group()
        } else null
    }

    @Throws(MalformedURLException::class)
    fun extractYoutubeId(url: String): String? {
//        val videoId = Global.extractYoutubeId("http://www.youtube.com/watch?v=t7UxjpUaL3Y")
//        val youtubveThumbNailUrl = "http://img.youtube.com/vi/$videoId/0.jpg"

        val query = URL(url).query
        val param = query.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var id: String? = null
        for (row in param) {
            val param1 = row.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (param1[0] == "v") {
                id = param1[1]
            }
        }
        return id
    }

    fun expand(v: View, nestedScrollView: NestedScrollView, tvSimilarProductLabel: TextView) {
        val matchParentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec((v.parent as View).width, View.MeasureSpec.EXACTLY)
        val wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    LinearLayout.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
                nestedScrollView.fullScroll(View.FOCUS_DOWN)
//                nestedScrollView.scrollTo(0, tvSimilarProductLabel.y.toInt())
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // Expansion speed of 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }

    fun expand(v: View) {
        val matchParentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec((v.parent as View).width, View.MeasureSpec.EXACTLY)
        val wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    LinearLayout.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // Expansion speed of 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // Collapse speed of 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }

    fun getAge(year: Int, month: Int, day: Int): String {
        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()
        dob.set(year, month, day)
        var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--
        }
        val ageInt = age
        return ageInt.toString()
    }

    fun getColorCode(colorName: String): String {
        val colors = StaticDataUtility.getColors()

        var selectedColor = ""
        for (v in 0 until colors.size) {
            if (colorName.toLowerCase() == colors[v].ID.toLowerCase()) {
                selectedColor = colors[v].title
            }
        }

        return selectedColor
    }
}