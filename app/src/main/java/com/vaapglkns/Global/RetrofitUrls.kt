package com.vaapglkns.Global

import com.google.gson.JsonObject
import com.vaapglkns.Global.RequestParamsUtils

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
interface RetrofitUrls {
    companion object {
        var MAIN_URL = "https://redblue.online/"
        var TEST_URL = "https://redblue.online/"
        var VAAPGLKNS = "https://redblue.online/xmlconnect/configuration/index/app_code/defand1/screen_size/480%C3%97800"
    }

    @GET("demos/view-flipper/data.php")
    fun homepage(): Call<ResponseBody>

//    @GET("/3/movie/top_rated?api_key=db3749e7bfc2840b77c347438bc2cc6d")
//    fun getMovieData(): Call<ResponseBody>

    @GET("movie/top_rated")
    fun getMovieData(@Query("api_key") apiKey: String): Call<ResponseBody>

    @GET("movie/{id}")
    fun getMovieDetails(@Path("id") id: Int, @Query("api_key") apiKey: String): Call<ResponseBody>

    @POST("restaurant/getAllRestaurantList")
    abstract fun getAllRestaurantList(@Body jsonObject: JsonObject): Call<ResponseBody>

    @POST("restaurant/getAllRestaurantList")
    fun bodyParameterDemo(@Body bodyParameter: Map<String, String>): Call<ResponseBody>

    @POST("restaurant/getAllRestaurantList")
    fun queryParameterDemo(@QueryMap bodyParameter: Map<String, String>): Call<ResponseBody>

    @POST("demo")
    fun headerAndBody(@HeaderMap headers: Map<String, String>, @Body bodyParameter: HashMap<String, String>): Call<ResponseBody>

    @POST(RequestParamsUtils.LOGIN)
    @FormUrlEncoded
    fun userLogin(@FieldMap bodyParameter: Map<String, String>): Call<ResponseBody>


    @POST("xmlconnect/wishlist/add/")
    @FormUrlEncoded
    fun addToWishList(@FieldMap bodyParameter: Map<String, String>): Call<ResponseBody>

    @POST("xmlconnect/customer/login")
    @FormUrlEncoded
    fun login(@FieldMap bodyParameter: Map<String, String>): Call<ResponseBody>

    @POST("xmlconnect/customer/form")
    @FormUrlEncoded
    fun loadLogin(@FieldMap bodyParameter: Map<String, String>): Call<ResponseBody>

    @POST("xmlconnect/customer/isLoggined/")
    @FormUrlEncoded
    fun isLoggedIn(@FieldMap bodyParameter: Map<String, String>): Call<ResponseBody>

    @POST("xmlconnect/customer/save")
    @FormUrlEncoded
    fun register(@FieldMap bodyParameter: Map<String, String>): Call<ResponseBody>

    @POST("xmlconnect/customer/logout/")
    @FormUrlEncoded
    fun logout(@FieldMap bodyParameter: Map<String, String>): Call<ResponseBody>

    @POST("xmlconnect/cart/add/")
    @FormUrlEncoded
    fun addToCart(@FieldMap bodyParameter: Map<String, String>): Call<ResponseBody>
}