package com.vaapglkns.Global

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
object SharedPreference {
    private var editor: SharedPreferences.Editor? = null

    @SuppressLint("CommitPrefEdits")
    fun CreatePreferences(context: Context, PreferenceName: String) {
        val sharedPreferences = context.getSharedPreferences(PreferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    fun SavePreferences(preferenceKey: String, preferenceVal: String) {
        editor!!.putString(preferenceKey, preferenceVal)
        editor!!.commit()
    }

    fun GetPreferences(context: Context, preferenceName: String, preferenceKey: String): String? {
        val sharedPreferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getString(preferenceKey, null)
    }

    fun ClearPreferences(context: Context, preferenceName: String) {
        val preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        preferences.edit().clear().apply()
    }

    fun RemovePreferences(context: Context, preferenceName: String, preferenceKey: String) {
        val settings = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        settings.edit().remove(preferenceKey).apply()
    }
}
