package com.vaapglkns.Global

import com.vaapglkns.Model.Spinner
import java.util.*

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
interface SpinnerCallback {
    abstract fun onDone(list: ArrayList<Spinner>)
}