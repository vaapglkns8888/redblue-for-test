package com.vaapglkns.Global

import android.os.Environment
import com.vaapglkns.Model.*
import com.vaapglkns.R
import java.io.File

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
internal object StaticDataUtility {
    val FINISH_ACTIVITY = "finish_activity"
    val PICK_IMAGE = "pick_image"
    const val DATE_FORMAT = "yyyy-MM-dd"
    const val TIME_FORMAT = "HH:mm:ss"

    val FOLDER_NAME = ".vaapglkns"
    val CACHE_DIR = ".vaapglkns/Cache"

    val TMP_DIR = (Environment
        .getExternalStorageDirectory().absolutePath
            + File.separator
            + FOLDER_NAME + "/tmp")

    val PATH = Environment.getExternalStorageDirectory()
        .absolutePath + File.separator + "" + FOLDER_NAME

    val FOLDER_RIDEINN_PATH = (Environment
        .getExternalStorageDirectory().absolutePath
            + File.separator
            + "vaapglkns")

    val API_KEY = "AIzaSyCsCzYT1hkwtjiJThhbi9m5Y3B179_-zgY"
    val USER_LATITUDE = "lat"
    val USER_LONGITUDE = "longi"
    //    val APP_JSON = "application/json"
    val APP_JSON = "application/x-www-form-urlencoded"
    var APP_CODE = "defand1"

    val LOGIN_INFO = "login_info"
    val PROFILE_INFO = "profile_info"
    val MASTER_DATA = "master_data"
    val COUNTRY = "country"

    val ROLE = "role"
    val ROLE_SELLER = 1
    val ROLE_BUYER = 2
    val ROLE_GENERAL = 3

    val LDPI = 0.75
    val MDPI = 1.0
    val HDPI = 1.5
    val XHDPI = 2.0
    val XXHDPI = 3.0
    val XXXHDPI = 4.0

    val PROFILE = "profile"
    val ANDROID_TYPE = 1
    val TIMEOUT = 5 * 60 * 1000
    val LOCATION_UPDATED = "location_updated"
    val REQ_CODE_SETTING = 555
    val REQ_CAPTURE_IMAGE = 8887
    val REQUEST_VIDEO_CAPTURE = 8888
    val REQ_PICK_IMAGE = 8889
    const val REQUEST_PERMISSION = 101
    const val REQUEST_GPS = 8811

    val TAG_ADD = "add"
    val TAG_CANCEL = "cancel"
    val TAG_REMOVE = "remove"
    val TAG_REJECT = "reject"
    val TAG_BLOCK = "block"
    val TAG_UNBLOCK = "unblock"
    val TAG_ACCEPT = "accept"
    val ZIP_CODE = "zipcode"
    val CART_DATA = "cart_data"
    val LANGUAGE = "language"
    val REFERSH_HOMEPAGE = "refersh_homepage"
    val COUNTRY_CODE = "country_code"
    val IS_EMAIL_CONFIRM = "is_email_confirm"
    val IS_ZIP_AVAILABLE = "is_zip_available"
    val ADDRESS_PRESELECT = "address_preselect"


    //FOR SHARED PREFERENCES
    val sVAAPGLKNS_PREFERENCES = "sVAAPGLKNS_PREFERENCES"
    val sUSER_TOKEN = "sUSER_TOKEN"
    val sUSER_ID = "sUSER_ID"
    val sIS_AUTO_START_PERMISSION_ENABLED = "sIS_AUTO_START_PERMISSION_ENABLED"


    //FOR BUNDLE PARAMETER
    val bURL = "url"
    val bTITLE = "title"
    val bPRODUCT_NAME = "bPRODUCT_NAME"
    val bUNIT_PRICE = "bUNIT_PRICE"
    val bSPECIAL_PRICE = "bSPECIAL_PRICE"
    val bPRODUCT_IMAGE = "bPRODUCT_IMAGE"
    val bPRODUCT_DESCRIPTION = "bPRODUCT_DESCRIPTION"
    val bPRODUCT_LINK = "bPRODUCT_LINK"
    val bADDRESS_ITEM_CLASS = "bADDRESS_ITEM_CLASS"
    val bMY_ORDERS_ITEM_CLASS = "bMY_ORDERS_ITEM_CLASS"
    val bORDER_REVIEW_ITEM_CLASS = "bORDER_REVIEW_ITEM_CLASS"
    val bIS_FROM_DRAWER_MENU = "bIS_FROM_DRAWER_MENU"
    val bCATEGORY_ID = "bCATEGORY_ID"
    val bCATEGORY_NAME = "bCATEGORY_NAME"
    val bID = "bID"


    //FOR WEB-VIEW
    val QUERY_PLACE_HOLDER = "%s"
    val DUCK_DUCK_GO_SEARCH_URL = "https://duckduckgo.com/?q=$QUERY_PLACE_HOLDER"
    val DUCK_DUCK_GO_LITE_SEARCH_URL = "https://duckduckgo.com/lite/?q=$QUERY_PLACE_HOLDER"
    val GOOGLE_SEARCH_URL = "https://www.google.com/search?ie=UTF-8&oe=UTF-8&q=$QUERY_PLACE_HOLDER"
    val YAHOO_SEARCH_URL = "https://search.yahoo.com/search?p=$QUERY_PLACE_HOLDER"
    val BING_SEARCH_URL = "https://www.bing.com/search?q=$QUERY_PLACE_HOLDER"
    val ASK_SEARCH_URL = "http://www.ask.com/web?qsrc=0&o=0&l=dir&q=$QUERY_PLACE_HOLDER"
    val START_PAGE_SEARCH_URL = "https://startpage.com/do/search?language=english&query=$QUERY_PLACE_HOLDER"
    val START_PAGE_MOBILE_SEARCH_URL =
        "https://startpage.com/do/m/mobilesearch?language=english&query=$QUERY_PLACE_HOLDER"
    val BAIDU_SEARCH_URL = "https://www.baidu.com/s?wd=$QUERY_PLACE_HOLDER"
    val YANDEX_SEARCH_URL = "https://yandex.ru/yandsearch?lr=21411&text=$QUERY_PLACE_HOLDER"
    val NAVER_SEARCH_URL = "https://search.naver.com/search.naver?ie=utf8&query=$QUERY_PLACE_HOLDER"

    // region FOR GET STATES
    fun getStates(): ArrayList<Spinner> {
        val data: java.util.ArrayList<Spinner> = java.util.ArrayList()
        data.add(Spinner("0", "Andaman and Nicobar"))
        data.add(Spinner("1", "Andhra Pradesh"))
        data.add(Spinner("2", "Arunachal Pradesh"))
        data.add(Spinner("3", "Assam"))
        data.add(Spinner("4", "Bihar"))
        data.add(Spinner("5", "Chandigarh"))
        data.add(Spinner("6", "Chhattisgarh"))
        data.add(Spinner("7", "Dadra and Nagar Haveli"))
        data.add(Spinner("8", "Daman Diu"))
        data.add(Spinner("9", "Delhi"))
        data.add(Spinner("10", "Goa"))
        data.add(Spinner("11", "Gujarat"))
        data.add(Spinner("12", "Haryama"))
        data.add(Spinner("13", "Himachal Pradesh"))
        data.add(Spinner("14", "Jammu and Kashmir"))
        data.add(Spinner("15", "Jharkhand"))
        data.add(Spinner("16", "Karnataka"))
        data.add(Spinner("17", "Kerala"))
        data.add(Spinner("18", "Lakshadweep"))
        data.add(Spinner("19", "Madhya Pradesh"))
        data.add(Spinner("20", "Maharashtra"))
        data.add(Spinner("21", "Manipur"))
        data.add(Spinner("22", "Meghalaya"))
        data.add(Spinner("23", "Mizoram"))
        data.add(Spinner("24", "Nagaland"))
        data.add(Spinner("25", "Orissa"))
        data.add(Spinner("26", "Pondicherry"))
        data.add(Spinner("27", "Punjab"))
        data.add(Spinner("28", "Rajasthan"))
        data.add(Spinner("29", "Sikkim"))
        data.add(Spinner("30", "Tamil Nadu"))
        data.add(Spinner("31", "Tripura"))
        data.add(Spinner("32", "Uttar Pradesh"))
        data.add(Spinner("33", "Uttaranchal"))
        data.add(Spinner("34", "West Bengal"))

        return data
    }
    //endregion

    // region FOR GET COLOR CODE
    fun getColors(): ArrayList<Spinner> {
        val data: java.util.ArrayList<Spinner> = java.util.ArrayList()
        data.add(Spinner("black", "#000000"))
        data.add(Spinner("blackmelange", "#000000"))
        data.add(Spinner("blue", "#213d86"))
        data.add(Spinner("brightblue", "#0000ee"))
        data.add(Spinner("brown", "#a52a2a"))
        data.add(Spinner("Charcoal", "#575757"))
        data.add(Spinner("Charcoalgrey", "#676662"))
        data.add(Spinner("checks", "#dadded"))
        data.add(Spinner("cream", "#ffeccb"))
        data.add(Spinner("darkblue", "#134581"))
        data.add(Spinner("darkgreen", "#002810"))
        data.add(Spinner("Denimblue", "#3559a5"))
        data.add(Spinner("gold", "#ffb90f"))
        data.add(Spinner("green", "#056406"))
        data.add(Spinner("Grey", "#a8adb1"))
        data.add(Spinner("greycharcoal", "#bebebe"))
        data.add(Spinner("iceblue", "#def9fb"))
        data.add(Spinner("lemon", "#fffacd"))
        data.add(Spinner("lightblue", "#90a8b6"))
        data.add(Spinner("magenta", "#74113f"))
        data.add(Spinner("maroon", "#b03060"))
        data.add(Spinner("Multi", "#204863"))
        data.add(Spinner("mustard", "#fbc210"))
        data.add(Spinner("Navy", "#041531"))
        data.add(Spinner("orange", "#f0441c"))
        data.add(Spinner("Peach", "#f98171"))
        data.add(Spinner("pink", "#ffa6ac"))
        data.add(Spinner("Purple", "#660066"))
        data.add(Spinner("ramagreen", "#007c6d"))
        data.add(Spinner("Rani", "#ff004e"))
        data.add(Spinner("red", "#af1f29"))
        data.add(Spinner("royalblue", "#270ec2"))
        data.add(Spinner("ruby", "#dd0c60"))
        data.add(Spinner("seagreen", "#3cb371"))
        data.add(Spinner("Skin", "#fae393"))
        data.add(Spinner("teel", "#1ea8aa"))
        data.add(Spinner("tomato", "#ff6347"))
        data.add(Spinner("violet", "#db7093"))
        data.add(Spinner("White", "#FFFFFF"))
        data.add(Spinner("wine", "#641b30"))
        data.add(Spinner("Yellow", "#ffe900"))
        return data
    }
    //endregion
}