package com.vaapglkns.Global

import android.content.Context
import com.vaapglkns.Global.RequestParamsUtils
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
internal object RetrofitHttpRequest : OkHttpClient() {

    @Throws(IOException::class)
    fun newRequestRetrofit(context: Context): Retrofit {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val original = chain.request()

                //                        okhttp3.Response response = chain.proceed(original);
                //                        if (!response.isSuccessful()) {
                //
                //                            Debug.e("Failed","Failed");
                //
                //                            return response;
                //                        }

                val request = original.newBuilder()
                    .header(RequestParamsUtils.ACCESS_TOKEN, Global.getUserToken(context)!!)
                    .header(RequestParamsUtils.CONTENT_TYPE, StaticDataUtility.APP_JSON)
                    .header("Cookie", "frontend=ljghr81704qhsj001lja8gea13; frontend_cid=F1skAKKr60xMZKVk; external_no_cache=1; app_code=defand1; screen_size=480x800")
                    .method(original.method(), original.body())
                    .build()
                chain.proceed(request)
            }
            .build()

        if (Debug.DEBUG) {
            val builder = Retrofit.Builder()
                .baseUrl(RetrofitUrls.TEST_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
            return builder.build()
        } else {
            val builder = Retrofit.Builder()
                .baseUrl(RetrofitUrls.MAIN_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
            return builder.build()
        }
    }

    @Throws(IOException::class)
    fun newRequestRetrofit(context: Context, url: String): Retrofit {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val original = chain.request()

                //                        okhttp3.Response response = chain.proceed(original);

                //                        if (!response.isSuccessful()) {
                //
                //                            Debug.e("Failed","Failed");
                //
                //                            return response;
                //                        }

                val request = original.newBuilder()
                    .header(RequestParamsUtils.ACCESS_TOKEN, Global.getUserToken(context)!!)
                    .header(RequestParamsUtils.CONTENT_TYPE, StaticDataUtility.APP_JSON)
                    .method(original.method(), original.body())
                    .build()
                chain.proceed(request)
            }
            .build()

        val builder = Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
        return builder.build()
    }

    @Throws(IOException::class)
    fun newRequestRetrofit(okHttpClient: OkHttpClient): Retrofit {
        if (Debug.DEBUG) {
            val builder = Retrofit.Builder()
                .baseUrl(RetrofitUrls.TEST_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
            return builder.build()
        } else {
            val builder = Retrofit.Builder()
                .baseUrl(RetrofitUrls.MAIN_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
            return builder.build()
        }
    }

    @Throws(IOException::class)
    fun newRequestOkHttpClient(context: Context): OkHttpClient {
        return Builder()
            .addInterceptor { chain ->
                val original = chain.request()

                //                        okhttp3.Response response = chain.proceed(original);
                //                        if (!response.isSuccessful()) {
                //
                //                            Debug.e("Failed","Failed");
                //
                //                            return response;
                //                        }

                val request = original.newBuilder()
                    .header(RequestParamsUtils.ACCESS_TOKEN, Global.getUserToken(context)!!)
                    .header(RequestParamsUtils.CONTENT_TYPE, StaticDataUtility.APP_JSON)
                    .method(original.method(), original.body())
                    .build()
                chain.proceed(request)
            }
            .build()
    }
}