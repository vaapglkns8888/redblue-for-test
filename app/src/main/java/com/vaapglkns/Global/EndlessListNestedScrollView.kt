package com.vaapglkns.Global

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.opengl.ETC1.getHeight
import android.view.View
import androidx.core.widget.NestedScrollView


class EndlessListNestedScrollView(
    private val recyclerView: RecyclerView,
    mScrollView: NestedScrollView/*, private val mLinearLayoutManager: LinearLayoutManager*/
) {

    private var isLock = false
    private var enable = true
    private var stackFromEnd = false
    internal var firstVisibleItem: Int = 0
    internal var visibleItemCount: Int = 0
    internal var totalItemCount: Int = 0

    internal var onScrollListener: RecyclerView.OnScrollListener =
        object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (enable) {
                    //                if (stackFromEnd) {
                    //
                    //                    firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
                    //                    boolean loadMore = firstVisibleItem == 0;
                    //
                    //                    if (loadMore && !isLock) {
                    //                        if (loadMoreListener != null) {
                    //                            loadMoreListener.onLoadMore();
                    //                        }
                    //                    }
                    //
                    //                } else {
                    //                    visibleItemCount = recyclerView.getChildCount();
                    //                    totalItemCount = mLinearLayoutManager.getItemCount();
                    //                    firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
                    //                    boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                    //
                    //                    if (loadMore && !isLock) {
                    //                        if (loadMoreListener != null) {
                    //                            loadMoreListener.onLoadMore();
                    //                        }
                    //                    }
                    //                }

//                if (stackFromEnd) {
//                    firstVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition()
//                    totalItemCount = mLinearLayoutManager.itemCount
//                    val loadMore = firstVisibleItem == totalItemCount - 1
//
//                    if (loadMore && !isLock) {
//                        if (loadMoreListener != null) {
//                            loadMoreListener!!.onLoadMore()
//                        }
//                    }
//
//                } else {
//                    visibleItemCount = recyclerView.childCount
//                    totalItemCount = mLinearLayoutManager.itemCount
//                    firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()
//                    val loadMore = firstVisibleItem + visibleItemCount >= totalItemCount
//
//                    if (loadMore && !isLock) {
//                        if (loadMoreListener != null) {
//                            loadMoreListener!!.onLoadMore()
//                        }
//                    }
//                }
                }
            }
        }

    internal var loadMoreListener: OnLoadMoreListener? = null

    init {
//        this.recyclerView.setOnScrollListener(onScrollListener)
        mScrollView.viewTreeObserver.addOnScrollChangedListener {
            if (enable) {
                val view = mScrollView.getChildAt(mScrollView.childCount - 1) as View

                val diff = view.bottom - (mScrollView.height + mScrollView
                    .scrollY)

                if (diff == 0) {
                    if (!isLock) {
                        if (loadMoreListener != null) {
                            loadMoreListener!!.onLoadMore()
                        }
                    }
                }
            }
        }
    }

    fun setStackFromEnd(stackFromEnd: Boolean) {
        this.stackFromEnd = stackFromEnd
    }

    fun lockMoreLoading() {
        isLock = true
    }

    fun releaseLock() {
        isLock = false
    }

    fun disableLoadMore() {
        enable = false
    }

    fun enableLoadMore() {
        enable = true
    }

    fun setOnLoadMoreListener(loadMoreListener: OnLoadMoreListener) {
        this.loadMoreListener = loadMoreListener
    }

    interface OnLoadMoreListener {
        fun onLoadMore()
    }
}