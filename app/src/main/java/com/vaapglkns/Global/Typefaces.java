package com.vaapglkns.Global;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by jk on 3/12/2018.
 */

public class Typefaces {
    public static Typeface lightFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
    }

    public static Typeface regularFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
    }

    public static Typeface boldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
    }
}
