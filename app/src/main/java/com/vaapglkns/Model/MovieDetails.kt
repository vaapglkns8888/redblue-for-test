package com.vaapglkns.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MovieDetails {

    @SerializedName("adult")
    @Expose
    var adult: Boolean = false
    @SerializedName("backdrop_path")
    @Expose
    var backdropPath: String? = null
    @SerializedName("belongs_to_collection")
    @Expose
    var belongsToCollection: Any? = null
    @SerializedName("budget")
    @Expose
    var budget: Int = 0
    @SerializedName("genres")
    @Expose
    var genres: List<Genre> = ArrayList<Genre>()
    @SerializedName("homepage")
    @Expose
    var homepage: Any? = null
    @SerializedName("id")
    @Expose
    var id: Int = 0
    @SerializedName("imdb_id")
    @Expose
    var imdbId: String? = null
    @SerializedName("original_language")
    @Expose
    var originalLanguage: String? = null
    @SerializedName("original_title")
    @Expose
    var originalTitle: String? = null
    @SerializedName("overview")
    @Expose
    var overview: String? = null
    @SerializedName("popularity")
    @Expose
    var popularity: Float = 0.toFloat()
    @SerializedName("poster_path")
    @Expose
    var posterPath: String? = null
    @SerializedName("production_companies")
    @Expose
    var productionCompanies: List<ProductionCompany> = ArrayList<ProductionCompany>()
    @SerializedName("production_countries")
    @Expose
    var productionCountries: List<ProductionCountry> = ArrayList<ProductionCountry>()
    @SerializedName("release_date")
    @Expose
    var releaseDate: String? = null
    @SerializedName("revenue")
    @Expose
    var revenue: Int = 0
    @SerializedName("runtime")
    @Expose
    var runtime: Int = 0
    @SerializedName("spoken_languages")
    @Expose
    var spokenLanguages: List<SpokenLanguage> = ArrayList<SpokenLanguage>()
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("tagline")
    @Expose
    var tagline: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("video")
    @Expose
    var video: Boolean = false
    @SerializedName("vote_average")
    @Expose
    var voteAverage: Float = 0.toFloat()
    @SerializedName("vote_count")
    @Expose
    var voteCount: Int = 0

    inner class Genre {

        @SerializedName("id")
        @Expose
        var id: Int = 0
        @SerializedName("name")
        @Expose
        var name: String? = null

    }

    inner class ProductionCompany {

        @SerializedName("id")
        @Expose
        var id: Int = 0
        @SerializedName("logo_path")
        @Expose
        var logoPath: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("origin_country")
        @Expose
        var originCountry: String? = null

    }

    inner class ProductionCountry {

        @SerializedName("iso_3166_1")
        @Expose
        var iso31661: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null

    }

    inner class SpokenLanguage {

        @SerializedName("iso_639_1")
        @Expose
        var iso6391: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null

    }
}
