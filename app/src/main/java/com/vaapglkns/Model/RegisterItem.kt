package com.vaapglkns.Model


import com.google.gson.annotations.SerializedName

data class RegisterItem(
    @SerializedName("message")
    val message: Message
) {
    data class Message(
        @SerializedName("status")
        val status: String,
        @SerializedName("text")
        val text: String
    )
}