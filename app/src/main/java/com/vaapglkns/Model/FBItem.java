package com.vaapglkns.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FBItem {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("picture")
    @Expose
    public Picture picture;

    public class Picture {

        @SerializedName("data")
        @Expose
        public Data data;

    }

    public class Data {

        @SerializedName("is_silhouette")
        @Expose
        public boolean isSilhouette;
        @SerializedName("url")
        @Expose
        public String url;

    }
}
