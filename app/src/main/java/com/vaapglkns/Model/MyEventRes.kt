package com.vaapglkns.Model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class MyEventRes {

    @SerializedName("status")
    @Expose
    var status: Int = 0
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: List<Datum> = ArrayList()


    inner class Datum {

        @SerializedName("id")
        @Expose
        var id: Int = 0
        @SerializedName("user_id")
        @Expose
        var userId: Int = 0
        @SerializedName("event_name")
        @Expose
        var eventName: String? = null
        @SerializedName("event_start_time_date")
        @Expose
        var eventStartTimeDate: String? = null
        @SerializedName("event_end_time_date")
        @Expose
        var eventEndTimeDate: String? = null
        @SerializedName("venue")
        @Expose
        var venue: String? = null
        @SerializedName("pincode")
        @Expose
        var pincode: String? = null
        @SerializedName("city")
        @Expose
        var city: String? = null
        @SerializedName("state")
        @Expose
        var state: String? = null
        @SerializedName("country")
        @Expose
        var country: String? = null
        @SerializedName("description")
        @Expose
        var description: String? = null
        @SerializedName("latitude")
        @Expose
        var latitude: String? = null
        @SerializedName("longitude")
        @Expose
        var longitude: String? = null
        @SerializedName("featured_image")
        @Expose
        var featuredImage: String? = null
        @SerializedName("status")
        @Expose
        var status: String? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null
        @SerializedName("guest")
        @Expose
        var guest: Int = 0
        @SerializedName("yes")
        @Expose
        var yes: Int = 0
        @SerializedName("no")
        @Expose
        var no: Int = 0
        @SerializedName("maybe")
        @Expose
        var maybe: Int = 0
        @SerializedName("noreply")
        @Expose
        var noreply: Int = 0

    }

    //
}
