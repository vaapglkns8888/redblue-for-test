package com.vaapglkns.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Movie {

    @SerializedName("page")
    @Expose
    var page: Int = 0
    @SerializedName("total_results")
    @Expose
    var totalResults: Int = 0
    @SerializedName("total_pages")
    @Expose
    var totalPages: Int = 0
    @SerializedName("results")
    @Expose
    var results: List<Result> = ArrayList<Result>()

    inner class Result {

        @SerializedName("vote_count")
        @Expose
        var voteCount: Int = 0
        @SerializedName("id")
        @Expose
        var id: Int = 0
        @SerializedName("video")
        @Expose
        var video: Boolean = false
        @SerializedName("vote_average")
        @Expose
        var voteAverage: Float = 0.toFloat()
        @SerializedName("title")
        @Expose
        var title: String? = null
        @SerializedName("popularity")
        @Expose
        var popularity: Float = 0.toFloat()
        @SerializedName("poster_path")
        @Expose
        var posterPath: String? = null
        @SerializedName("original_language")
        @Expose
        var originalLanguage: String? = null
        @SerializedName("original_title")
        @Expose
        var originalTitle: String? = null
        @SerializedName("genre_ids")
        @Expose
        var genreIds: List<Int> = ArrayList()
        @SerializedName("backdrop_path")
        @Expose
        var backdropPath: String? = null
        @SerializedName("adult")
        @Expose
        var adult: Boolean = false
        @SerializedName("overview")
        @Expose
        var overview: String? = null
        @SerializedName("release_date")
        @Expose
        var releaseDate: String? = null
    }
}
