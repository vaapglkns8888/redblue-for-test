package com.vaapglkns.Model


import com.google.gson.annotations.SerializedName

data class SlidingBannerItems(
    @SerializedName("home_banners")
    val homeBanners: HomeBanners
) {
    data class HomeBanners(
        @SerializedName("item")
        val item: List<Item>
    ) {
        data class Item(
            @SerializedName("image")
            val image: Image,
            @SerializedName("entity_id")
            val entityId: String,
            @SerializedName("type")
            val type: String
        ) {
            data class Image(
                @SerializedName("content")
                val content: String,
                @SerializedName("modification_time")
                val modificationTime: String
            )
        }
    }
}