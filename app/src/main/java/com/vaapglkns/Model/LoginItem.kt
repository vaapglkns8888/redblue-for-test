package com.vaapglkns.Model


import com.google.gson.annotations.SerializedName

data class LoginItem(
    @SerializedName("message")
    val message: Message
) {
    data class Message(
        @SerializedName("text")
        val text: String,
        @SerializedName("status")
        val status: String
    )
}