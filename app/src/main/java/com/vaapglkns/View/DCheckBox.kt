package com.vaapglkns.View

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatCheckBox
import com.vaapglkns.View.TextViewHelper

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
class DCheckBox : AppCompatCheckBox {
    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        TextViewHelper.setTypeface(context, this, attrs)
    }
}
