package com.vaapglkns.View

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatRadioButton
import com.vaapglkns.View.TextViewHelper

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
class DRadioButton : AppCompatRadioButton {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        TextViewHelper.setTypeface(context, this, attrs)
    }
}