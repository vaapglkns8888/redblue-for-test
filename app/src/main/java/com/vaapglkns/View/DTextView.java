package com.vaapglkns.View;

import android.content.Context;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
public class DTextView extends AppCompatTextView {
    public DTextView(Context context) {
        super(context);
    }

    public DTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TextViewHelper.INSTANCE.setTypeface(context, this, attrs);
    }
}
