package com.vaapglkns.View

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
    class DButton(context: Context, attrs: AttributeSet) : AppCompatButton(context, attrs) {

        init {
            TextViewHelper.setTypeface(context, this, attrs)
        }
}
