package com.vaapglkns.View

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import com.vaapglkns.Global.FontUtils
import com.vaapglkns.R

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
object TextViewHelper {

    fun setTypeface(context: Context, textView: TextView, attrs: AttributeSet) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.DTextView)
        var leftDrawable: Drawable? = null
        var topDrawable: Drawable? = null
        var typeface: Typeface? = null
        var rightDrawable: Drawable? = null
        var bottomDrawable: Drawable? = null

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leftDrawable = ta.getDrawable(R.styleable.DTextView_leftDrawable)
            rightDrawable = ta.getDrawable(R.styleable.DTextView_rightDrawable)
            bottomDrawable = ta.getDrawable(R.styleable.DTextView_bottomDrawable)
            topDrawable = ta.getDrawable(R.styleable.DTextView_topDrawable)
        } else {
            val drawableLeftId = ta.getResourceId(R.styleable.DTextView_leftDrawable, -1)
            val drawableRightId = ta.getResourceId(R.styleable.DTextView_rightDrawable, -1)
            val drawableBottomId = ta.getResourceId(R.styleable.DTextView_bottomDrawable, -1)
            val drawableTopId = ta.getResourceId(R.styleable.DTextView_topDrawable, -1)

            if (drawableLeftId != -1)
                leftDrawable = AppCompatResources.getDrawable(context, drawableLeftId)
            if (drawableRightId != -1)
                rightDrawable = AppCompatResources.getDrawable(context, drawableRightId)
            if (drawableBottomId != -1)
                bottomDrawable = AppCompatResources.getDrawable(context, drawableBottomId)
            if (drawableTopId != -1)
                topDrawable = AppCompatResources.getDrawable(context, drawableTopId)
        }

        val type = ta.getInt(R.styleable.DTextView_textFontFace, 1)
        typeface = FontUtils.fontName(context, type)

        if (leftDrawable != null || topDrawable != null || rightDrawable != null || bottomDrawable != null)
            textView.setCompoundDrawablesWithIntrinsicBounds(leftDrawable, topDrawable, rightDrawable, bottomDrawable)
        textView.typeface = typeface
        ta.recycle()
    }
}