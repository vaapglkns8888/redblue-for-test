package com.vaapglkns.View

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.vaapglkns.View.TextViewHelper

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
class DEditText : AppCompatEditText {

    val long: Long
        get() = if (length() > 0) java.lang.Long.parseLong(text!!.toString()) else 0

    val int: Int
        get() = if (length() > 0) Integer.parseInt(text!!.toString().trim { it <= ' ' }) else 0

    val string: String
        get() = if (length() > 0) text!!.toString().trim { it <= ' ' } else ""

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        TextViewHelper.setTypeface(context, this, attrs)
    }

    fun setText(data: Long) {
        if (data != 0L) {
            setText("" + data)
        } else {
            setText("0")
        }
    }

    fun setText(data: Long, def: Long) {
        if (data != 0L) {
            setText("" + data)
        } else {
            setText("" + def)
        }
    }

    fun setText(data: String?) {
        if (data != null && !data.equals("", ignoreCase = true)) {
            super.setText(data)
        } else {
            super.setText("")
        }
    }

    fun setText(data: String?, def: String) {
        if (data != null && !data.equals("", ignoreCase = true)) {
            setText(data)
        } else {
            setText(def)
        }
    }
}
