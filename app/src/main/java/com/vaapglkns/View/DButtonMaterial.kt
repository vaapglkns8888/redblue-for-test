package com.vaapglkns.View

import android.content.Context
import android.util.AttributeSet

/**
 * Created by VAAPGLKNS on 14-Jun-18.
 */
class DButtonMaterial(context: Context, attrs: AttributeSet) : com.rey.material.widget.Button(context, attrs) {

    init {
        TextViewHelper.setTypeface(context, this, attrs)
    }
}